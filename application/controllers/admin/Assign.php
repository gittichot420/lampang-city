<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Assign extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (empty($this->session->userdata('users'))) {
            redirect(base_url());
        }

        $this->load->model('admin/assign_model', 'assign');
        $this->load->model('admin/group_model', 'group');
        $this->load->model('admin/sub_group_model', 'sub_group');
    }

    public function g_name($g_id = [])
    {
        $g_name = $this->assign->g_name($g_id);
        $g_format = '';
        foreach ($g_name as $value) {
            $g_format .= '- ' . $value['g_name'] . '<br>';
        }
        return $g_format;
    }

    public function index()
    {
        $get_admin = $this->assign->get_admin();
        $i = 0;
        $arr = [];
        $admin_id = [];
        foreach ($get_admin as $key => $value) {
            $arr[] = explode("/", $value['as_sub_id']);
            for ($j = 0; $j < count($arr); $j++) {
                $g_name = $this->g_name($arr[$j]);
                $g_id[$j] = $arr[$j];
            }
            $get_admin[$key]['g_name'] = $g_name;
            $admin_id[] = $value['m_id'];
            $i++;
        }

        $group_id = [];
        for ($y = 0; $y < count($arr); $y++) {
            for ($m = 0; $m < count($arr[$y]); $m++) {
                array_push($group_id, $arr[$y][$m]);
            }
        }

        if ($group_id != null) {
            $this->data['get_group'] = $this->assign->get_group($group_id);
        } else {
            $this->data['get_group'] = $this->assign->get_groupALL();
        }
        if ($admin_id != null) {
            $this->data['get_member'] = $this->assign->get_member($admin_id);
        } else {
            $this->data['get_member'] = $this->assign->get_as_admin();
        }


        $this->data['get_admin'] = $get_admin;
        $this->data['group_id'] = $group_id;
        $this->load->view('admin/theme/header');
        $this->load->view('admin/assign/index', $this->data);
        $this->load->view('admin/assign/modal');
        $this->load->view('admin/theme/footer');
    }

    public function create()
    {
        if ($this->input->post()) {
            $m_id = $this->input->post('mem_id');
            $group_list = $this->input->post('group_list');
            $group_id = '';
            for ($i = 0; $i < count($group_list); $i++) {
                if ($i == count($group_list) - 1) {
                    $group_id .= $group_list[$i];
                } else {
                    $group_id .= $group_list[$i] . '/';
                }
            }
            $data_insert = [
                'as_sub_id' => $group_id,
                'as_m_id' => $m_id
            ];
            $result = $this->assign->add_assign($data_insert);
            if ($result == 'success') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'กำหนดสิทธิ์ผู้รับผิดชอบสำเร็จ.');
                redirect(base_url('admin/assign'));
            } else if ($result == 'false') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'กำหนดสิทธิ์ผู้รับผิดชอบไม่สำเร็จ.');
                redirect(base_url('admin/assign'));
            }
        } else {
            redirect(base_url('admin/assign'));
        }
    }

    public function delete($id = NULL)
    {
        if ($id != null) {
            $result = $this->assign->del_assign($id);
            if ($result == 'success') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'ลบข้อมูลผู้รับผิดชอบสำเร็จ.');
                redirect(base_url('admin/assign'));
            } else if ($result == 'false') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'ลบข้อมูลผู้รับผิดชอบไม่สำเร็จ.');
                redirect(base_url('admin/assign'));
            }
        } else {
            redirect(base_url('admin/assign'));
        }
    }
}