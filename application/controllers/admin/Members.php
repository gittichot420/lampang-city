<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Members extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (empty($this->session->userdata('users'))) {
            redirect(base_url());
        }
        $this->load->model('admin/members_model', 'members');
    }

    public function password_hash($password)
    {
        return md5($password);
    }

    public function index()
    {
        $this->data['get_users'] = $this->members->get_members();

        $this->load->view('admin/theme/header');
        $this->load->view('admin/members/index', $this->data);
        $this->load->view('admin/theme/footer');
    }

    public function add()
    {
        if ($this->input->post()) {

            $card_id = $this->input->post('card_id');
            $title = $this->input->post('title');
            $fname = $this->input->post('fname');
            $lname = $this->input->post('lname');
            $position = $this->input->post('position');
            $email = $this->input->post('email');
            $confirm_email = $this->input->post('confirm_email');
            $password = $this->input->post('password');
            $confirm_password = $this->input->post('confirm_password');
            $pass_hash = $this->password_hash($password);

            if (empty($card_id) || empty($title) || empty($fname) || empty($lname) || empty($position) || empty($email) || empty($confirm_email) || empty($password) || empty($confirm_password)) {
                $this->session->set_flashdata('result', 'false');
                $this->session->set_flashdata('message', 'กรุณากรอกข้อมูลให้ครับถ้วน1.');
                redirect(base_url('register'));
            }
            if ($email != $confirm_email) {
                $this->session->set_flashdata('result', 'false');
                $this->session->set_flashdata('message', 'กรุณากรอกข้อมูลให้ครับถ้วน2.');
                redirect(base_url('register'));
            }
            if ($password != $confirm_password) {
                $this->session->set_flashdata('result', 'false');
                $this->session->set_flashdata('message', 'กรุณากรอกข้อมูลให้ครับถ้วน3.');
                redirect(base_url('register'));
            }

            $data_insert = [
                'u_user' => $card_id,
                'u_realpass' => $password,
                'u_pass' => $pass_hash,
                'u_email' => $email,
                'u_title' => $title,
                'u_fname' => $fname,
                'u_lname' => $lname,
                'u_position' => $position,
                'u_status' => 1,
            ];

            $result = $this->members->add_member($data_insert);
            if ($result == 'success') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'สมัครบัญชีผู้ใช้สำเร็จ.');
                redirect(base_url('admin/members'));
            } else if ($result == 'false') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'สมัครบัญชีผู้ใช้ไม่สำเร็จ.');
                redirect(base_url('admin/members/add'));
            }
        } else {
            $this->data['page'] = 'members';

            $this->load->view('admin/theme/header', $this->data);
            $this->load->view('admin/members/add', $this->data);
            $this->load->view('admin/theme/footer');
        }
    }

    public function edit($id = null)
    {
        if ($this->input->get()) {
            $card_id = $this->input->get('card_id');
            $title = $this->input->get('title');
            $fname = $this->input->get('fname');
            $lname = $this->input->get('lname');
            $position = $this->input->get('position');
            $email = $this->input->get('email');
            $status = ($this->input->get('status')) ? $this->input->get('status') : '0';

            if (!empty($this->input->get('password'))) {
                $password = $this->input->get('password');
                $confirm_password = $this->input->get('confirm_password');
                $pass_hash = $this->password_hash($password);

                if ($password != $confirm_password) {
                    $this->session->set_flashdata('result', 'false');
                    $this->session->set_flashdata('message', 'กรุณากรอกข้อมูลให้ครับถ้วนและถูกต้อง.');
                    redirect(base_url('admin/members/edit/' . $id));
                }
                $data_insert = [
                    'u_user' => $card_id,
                    'u_realpass' => $password,
                    'u_pass' => $pass_hash,
                    'u_email' => $email,
                    'u_title' => $title,
                    'u_fname' => $fname,
                    'u_lname' => $lname,
                    'u_position' => $position,
                    'u_status' => $status,
                ];
            } else {
                $data_insert = [
                    'u_user' => $card_id,
                    'u_email' => $email,
                    'u_title' => $title,
                    'u_fname' => $fname,
                    'u_lname' => $lname,
                    'u_position' => $position,
                    'u_status' =>  $status,
                ];
            }

            $result = $this->members->update_users($id, $data_insert);
            if ($result == 'success') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'แก้ไขบัญชีผู้ใช้สำเร็จ.');
                redirect(base_url('admin/members'));
            } else if ($result == 'false') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'แก้ไขบัญชีผู้ใช้ไม่สำเร็จ.');
                redirect(base_url('admin/members/edit/' . $id));
            }
        } else {
            $this->data['page'] = 'members';
            $this->data['users_id'] = $this->members->get_users_id($id);
            $this->load->view('admin/theme/header', $this->data);
            $this->load->view('admin/members/edit', $this->data);
            $this->load->view('admin/theme/footer');
        }
    }

    public function del($id = null)
    {
        if ($id != null) {
            $result = $this->members->del_members($id);
            if ($result == 'success') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'ลบข้อมูลผู้ใช้งานสำเร็จ.');
                redirect(base_url('admin/members'));
            } else if ($result == 'false') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'ลบข้อมูลผู้ใช้งานไม่สำเร็จ.');
                redirect(base_url('admin/members'));
            }
        } else {
            redirect(base_url('admin/members'));
        }
    }
}
