<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Group extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (empty($this->session->userdata('users'))) {
            redirect(base_url());
        }
        $this->load->model('admin/group_model', 'group');
        $this->load->model('admin/sub_group_model', 'sub_group');
    }

    public function index()
    {
        $this->data['get_group'] = $this->group->get_group();

        $this->load->view('admin/theme/header', $this->data);
        $this->load->view('admin/group/index', $this->data);
        $this->load->view('admin/group/modal');
        $this->load->view('admin/theme/footer');
    }

    public function create()
    {
        if ($this->input->post()) {
            $g_name = $this->input->post('groups');
            $data_group = $this->group->group_check($g_name);
            if ($data_group == NULL) {
                $data_insert = [
                    'g_name' => $g_name
                ];
                $result = $this->group->add_group($data_insert);
                if ($result == 'success') {
                    $this->session->set_flashdata('result', $result);
                    $this->session->set_flashdata('message', 'เพิ่มข้อมูลสำนักสำเร็จ.');
                    redirect(base_url('admin/group'));
                } else if ($result == 'false') {
                    $this->session->set_flashdata('result', $result);
                    $this->session->set_flashdata('message', 'เพิ่มข้อมูลสำนักไม่สำเร็จ.');
                    redirect(base_url('admin/group'));
                }
            } else {
                $this->session->set_flashdata('result', 'duplicate');
                $this->session->set_flashdata('message', 'ข้อมูลสำนักนี้มีอยู่ในระบบแล้ว.');
                redirect(base_url('admin/group'));
            }
        } else {
            redirect(base_url('admin/group'));
        }
    }

    public function update()
    {
        if ($this->input->get()) {
            $id = $this->input->get('g_id');
            $g_name = $this->input->get('g_name');
            $data_insert = [
                'g_name' => $g_name,
            ];
            $data_group = $this->group->group_check($g_name);
            if ($data_group == NULL) {
                $result = $this->group->update_group($id, $data_insert);
                if ($result == 'success') {
                    $this->session->set_flashdata('result', $result);
                    $this->session->set_flashdata('message', 'แก้ไขข้อมูลสำนักสำเร็จ.');
                    redirect(base_url('admin/group'));
                } else if ($result == 'false') {
                    $this->session->set_flashdata('result', $result);
                    $this->session->set_flashdata('message', 'แก้ไขข้อมูลสำนักไม่สำเร็จ.');
                    redirect(base_url('admin/group'));
                }
            } else if ($data_group['g_id'] == $id) {
                $result = $this->group->update_group($id, $data_insert);
                if ($result == 'success') {
                    $this->session->set_flashdata('result', $result);
                    $this->session->set_flashdata('message', 'แก้ไขข้อมูลสำนักสำเร็จ.');
                    redirect(base_url('admin/group'));
                } else if ($result == 'false') {
                    $this->session->set_flashdata('result', $result);
                    $this->session->set_flashdata('message', 'แก้ไขข้อมูลสำนักไม่สำเร็จ.');
                    redirect(base_url('admin/group'));
                }
            } else {
                $this->session->set_flashdata('result', 'duplicate');
                $this->session->set_flashdata('message', 'ข้อมูลสำนักนี้มีอยู่ในระบบแล้ว.');
                redirect(base_url('admin/group'));
            }
        } else {
            redirect(base_url('admin/group'));
        }
    }

    public function delete()
    {
        if ($this->input->get()) {
            $id = $this->input->get('g_id');
            $result = $this->group->del_group($id);
            if ($result == 'success') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'ลบข้อมูลสำนักสำเร็จ.');
                redirect(base_url('admin/group'));
            } else if ($result == 'false') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'ลบข้อมูลสำนักไม่สำเร็จ.');
                redirect(base_url('admin/group'));
            }
        } else {
            redirect(base_url('admin/group'));
        }
    }
}