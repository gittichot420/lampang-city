<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Category extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (empty($this->session->userdata('users'))) {
            redirect(base_url());
        }
        $this->load->model('admin/category_model', 'category');
        $this->load->model('admin/sub_group_model', 'sub_group');
    }

    public function index()
    {
        $this->data['get_category'] = $this->category->get_category();

        $this->load->view('admin/theme/header');
        $this->load->view('admin/category/index', $this->data);
        $this->load->view('admin/category/modal');
        $this->load->view('admin/theme/footer');
    }

    public function create()
    {
        if ($this->input->post()) {
            $cat_name = $this->input->post('cat_name');
            $cat_type = $this->input->post('cat_type');
            $data_cat = $this->category->category_check($cat_name);
            if ($data_cat == NULL) {
                $data_insert = [
                    'cat_name' => $cat_name,
                    'cat_type' => $cat_type,
                ];
                $result = $this->category->add_category($data_insert);
                if ($result == 'success') {
                    $this->session->set_flashdata('result', $result);
                    $this->session->set_flashdata('message', 'เพิ่มข้อมูประเภทสัญญาจ้างสำเร็จ.');
                    redirect(base_url('admin/category'));
                } else if ($result == 'false') {
                    $this->session->set_flashdata('result', $result);
                    $this->session->set_flashdata('message', 'เพิ่มข้อมูประเภทสัญญาจ้างไม่สำเร็จ.');
                    redirect(base_url('admin/category'));
                }
            } else {
                $this->session->set_flashdata('result', 'duplicate');
                $this->session->set_flashdata('message', 'ข้อมูประเภทสัญญาจ้างนี้มีอยู่ในระบบแล้ว.');
                redirect(base_url('admin/category'));
            }
        } else {
            redirect(base_url('admin/category'));
        }
    }

    public function update()
    {
        if ($this->input->get()) {
            $id = $this->input->get('cat_id');
            $cat_type = $this->input->get('cat_type');
            $cat_name = $this->input->get('cat_name');
            $data_insert = [
                'cat_name' => $cat_name,
                'cat_type' => $cat_type,
            ];

            $data_category = $this->category->category_check($cat_name);
            if ($data_category == NULL) {
                $result = $this->category->update_category($id, $data_insert);
                if ($result == 'success') {
                    $this->session->set_flashdata('result', $result);
                    $this->session->set_flashdata('message', 'แก้ไขข้อมูลประเภทสัญญาจ้างสำเร็จ.');
                    redirect(base_url('admin/category'));
                } else if ($result == 'false') {
                    $this->session->set_flashdata('result', $result);
                    $this->session->set_flashdata('message', 'แก้ไขข้อมูลประเภทสัญญาจ้างไม่สำเร็จ.');
                    redirect(base_url('admin/category'));
                }
            } else if ($data_category['cat_id'] == $id) {
                $result = $this->category->update_category($id, $data_insert);
                if ($result == 'success') {
                    $this->session->set_flashdata('result', $result);
                    $this->session->set_flashdata('message', 'แก้ไขข้อมูลประเภทสัญญาจ้างสำเร็จ.');
                    redirect(base_url('admin/category'));
                } else if ($result == 'false') {
                    $this->session->set_flashdata('result', $result);
                    $this->session->set_flashdata('message', 'แก้ไขข้อมูลประเภทสัญญาจ้างไม่สำเร็จ.');
                    redirect(base_url('admin/category'));
                }
            } else {
                $this->session->set_flashdata('result', 'duplicate');
                $this->session->set_flashdata('message', 'ข้อมูลประเภทสัญญาจ้างนี้มีอยู่ในระบบแล้ว.');
                redirect(base_url('admin/category'));
            }
        } else {
            redirect(base_url('admin/category'));
        }
    }

    public function delete()
    {
        if ($this->input->get()) {
            $id = $this->input->get('cat_id');
            $result = $this->category->del_category($id);
            if ($result == 'success') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'ลบข้อมูลประเภทสัญญาจ้างสำเร็จ.');
                redirect(base_url('admin/category'));
            } else if ($result == 'false') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'ลบข้อมูลประเภทสัญญาจ้างไม่สำเร็จ.');
                redirect(base_url('admin/category'));
            }
        } else {
            redirect(base_url('admin/category'));
        }
    }
}