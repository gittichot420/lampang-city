<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Slip extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (empty($this->session->userdata('users'))) {
            redirect(base_url());
        }

        $this->load->model('admin/assign_model', 'assign');
        $this->load->model('admin/group_model', 'group');
        $this->load->model('admin/slip_model', 'slip');
        $this->load->model('admin/users_model', 'users');
        $this->load->model('admin/sub_group_model', 'sub_group');
    }

    public function index($g_id = null)
    {
        $month = $this->input->get('month');
        if (empty($month)) {
            $month = date('Y-m');
        }
        if ($g_id == '888888' || $g_id == '999999') {
            if ($g_id == '888888') {
                $type = 1;
            } else if ($g_id == '999999') {
                $type = 3;
            }
            if ($type == 0) {
                $type_name = 'ข้าราชการดำรงตำแหน่ง';
            } else if ($type == 1) {
                $type_name = 'ข้าราชการบำนาญ';
            } else if ($type == 2) {
                $type_name = 'พนักงานจ้าง';
            } else if ($type == 3) {
                $type_name = 'ผู้บริหาร';
            }

            $user_type = $this->slip->get_user_type($type);
            foreach ($user_type as $key => $value) {
                $total = $this->slip_total($value['m_id'], $month);
                $grand_expense = $this->grand_expense($value['m_id'], $month);
                $grand_total = $this->grand_total($value['m_id'], $month);
                $user_type[$key]['total'] = $total;
                $user_type[$key]['grand_expense'] = $grand_expense;
                $user_type[$key]['grand_total'] = $grand_total;
            }

            $this->data['month'] = $month;
            $this->data['type_name'] = $type_name;
            $this->data['page'] = 'dashboard';
            $this->data['user_type'] = $user_type;

            $this->load->view('admin/theme/header', $this->data);
            $this->load->view('admin/slip/specifically', $this->data);
            $this->load->view('admin/theme/footer');
        } else {

            $mem_id = $this->session->userdata('users')['m_id'];
            $assign = $this->assign->assign_id($mem_id);
            $assign_id = explode(",", $assign['as_sub_id']);
            $this->data['group_assign'] = $this->assign->group_assign($assign_id);
            $this->data['g_id'] = $g_id;
            $this->data['page'] = 'dashboard';

            $this->load->view('admin/theme/header', $this->data);
            $this->load->view('admin/slip/index', $this->data);
            $this->load->view('admin/theme/footer');
        }
    }

    public function slip_total($id = null, $month = null)
    {
        $slip_type = $this->slip->get_slip_type($id, $month);
        if (!empty($slip_type)) {
            return $slip_type['total'];
        } else {
            return 0;
        }
    }
    public function grand_expense($id = null, $month = null)
    {
        $slip_type = $this->slip->get_slip_type($id, $month);
        if (!empty($slip_type)) {
            return $slip_type['grand_expense'];
        } else {
            return 0;
        }
    }
    public function grand_total($id = null, $month = null)
    {
        $slip_type = $this->slip->get_slip_type($id, $month);
        if (!empty($slip_type)) {
            return $slip_type['grand_total'];
        } else {
            return 0;
        }
    }

    public function slip_all()
    {
        $m_id = $this->input->post('m_id');
        $salary = $this->input->post('salary');
        $month = $this->input->post('month');
        $arrID = explode("/", $m_id);
        $arrSalary = explode("/", $salary);
        $beforeMonth =  date('Y-m', strtotime($month . ' -1 month'));
        $data_users = $this->users->get_users_id($arrID[0]);

        if ($data_users['cat_type'] == 0) {
            $base_url = base_url('admin/slip-admin/' . $data_users['g_id'] . '/' . $data_users['cat_type'] . '?month=' . $month);
        } else if ($data_users['cat_type'] == 1) {
            $base_url = base_url('admin/dashboard/888888?month=' . $month);
        } else if ($data_users['cat_type'] == 2) {
            $base_url = base_url('admin/slip-admin/' . $data_users['g_id'] . '/' . $data_users['cat_type'] . '?month=' . $month);
        } else if ($data_users['cat_type'] == 3) {
            $base_url = base_url('admin/dashboard/999999?month=' . $month);
        }

        $month_name = $this->getMonthNmae($month);
        $_allBefore = $this->slip->allBefore($beforeMonth, $arrID);

        if (!empty($_allBefore)) {
            for ($i = 0; $i < count($_allBefore); $i++) {
                unset($_allBefore[$i]['slip_id']);
                $_allBefore[$i]['slip_date'] = $month;
                $_allBefore[$i]['created_at'] = date('Y-m-d H:s:i');
            }
            for ($i = 0; $i < count($_allBefore); $i++) {
                $result = $this->slip->addAll_slip($_allBefore[$i]);
            }
        } else {
            // flase
            $in_come = '';
            for ($i = 1; $i <= 6; $i++) {
                if ($i == 6) {
                    $in_come .= 0;
                } else {
                    $in_come .= 0 . '/';
                }
            }
            $expense = '';
            for ($i = 1; $i <= 46; $i++) {
                if ($i == 46) {
                    $expense .= 0;
                } else {
                    $expense .= 0 . '/';
                }
            }

            for ($i = 0; $i < count($arrID); $i++) {
                $data_insert[$i] = [
                    'slip_date' => $month,
                    'm_id' => $arrID[$i],
                    'salary' => $this->format_float($arrSalary[$i]),
                    'in_come' => $in_come,
                    'total' => $this->format_float($arrSalary[$i]),
                    'expense' => $expense,
                    'grand_expense' => 0,
                    'grand_total' => $this->format_float($arrSalary[$i]),
                    'staff_id' => $this->session->userdata('users')['m_id'],
                    'slip_status' => 1,
                    'created_at' => date('Y-m-d H:s:i'),
                ];
                $result = $this->slip->addAll_slip($data_insert[$i]);
            }
        }


        if ($result == 'success') {
            $this->session->set_flashdata('result', $result);
            $this->session->set_flashdata('message', 'จัดการรายการเงินเดือนประจำเดือน ' . $month_name . ' ทั้งหมดสำเร็จ .');
            redirect($base_url);
        } else if ($result == 'false') {
            $this->session->set_flashdata('result', $result);
            $this->session->set_flashdata('message', 'จัดการรายการเงินเดือนประจำเดือน ' . $month_name . ' ทั้งหมดไม่สำเร็จ .');
            redirect($base_url);
        }
    }

    public function getMonthNmae($month = null)
    {
        $year_name = date("Y", strtotime($month)) + 543;
        $month_id = date("m", strtotime($month));

        $month_name = array(
            "",
            "มกราคม",
            "กุมภาพันธ์",
            "มีนาคม",
            "เมษายน",
            "พฤษภาคม",
            "มิถุนายน",
            "กรกฎาคม",
            "สิงหาคม",
            "กันยายน",
            "ตุลาคม",
            "พฤศจิกายน",
            "ธันวาคม"
        );
        return $month_name[(int) $month_id] . ' ' . $year_name;
    }

    public function slip_admin($group = null, $type = null)
    {
        $month = $this->input->get('month');
        if (empty($month)) {
            $month = date('Y-m');
        }

        $user_type = $this->slip->userGroupType($group, $type);
        if ($type == 0) {
            $type_name = 'ข้าราชการดำรงตำแหน่ง';
        } else if ($type == 1) {
            $type_name = 'ข้าราชการบำนาญ';
        } else if ($type == 2) {
            $type_name = 'พนักงานจ้าง';
        } else if ($type == 3) {
            $type_name = 'ผู้บริหาร';
        }

        foreach ($user_type as $key => $value) {
            $total = $this->slip_total($value['m_id'], $month);
            $grand_expense = $this->grand_expense($value['m_id'], $month);
            $grand_total = $this->grand_total($value['m_id'], $month);
            $user_type[$key]['total'] = $total;
            $user_type[$key]['grand_expense'] = $grand_expense;
            $user_type[$key]['grand_total'] = $grand_total;
        }

        $this->data['group'] = $group;
        $this->data['month'] = $month;
        $this->data['type'] = $type;
        $this->data['type_name'] = $type_name;
        $this->data['page'] = 'dashboard';
        $this->data['user_type'] = $user_type;

        $this->load->view('admin/theme/header', $this->data);
        $this->load->view('admin/slip/specifically', $this->data);
        $this->load->view('admin/theme/footer');
    }

    public function format_float($number = null)
    {
        return floatval(preg_replace('/[^\d\.]/', '', $number));
    }

    public function slip_add($month = null, $mem_id = null)
    {
        $this->data['slip_admin'] = $this->slip->get_mem($mem_id);
        $this->data['month_name'] = $this->getMonthNmae($month);
        $this->data['mem_id'] = $mem_id;
        $this->data['month'] = $month;

        $this->load->view('admin/theme/header', $this->data);
        $this->load->view('admin/slip/add', $this->data);
        $this->load->view('admin/theme/footer');
        // }
    }

    public function slip_update($month = null, $mem_id = null)
    {
        $this->data['slip_admin'] = $this->slip->slip_m_id($month, $mem_id);
        $this->data['month_name'] = $this->getMonthNmae($month);
        $this->data['month'] = $month;
        $this->data['mem_id'] = $mem_id;

        $this->load->view('admin/theme/header', $this->data);
        $this->load->view('admin/slip/edit', $this->data);
        $this->load->view('admin/theme/footer');
    }

    public function add($id = null)
    {
        if ($this->input->post()) {

            $cat_type = $this->input->post('cat_type');
            $s_date = $this->input->post('s_date');

            $in_come = '';
            for ($i = 1; $i <= 6; $i++) {
                if ($i == 6) {
                    $in_come .= $this->format_float($this->input->post('in_come' . $i), 2);
                } else {
                    $in_come .= $this->format_float($this->input->post('in_come' . $i), 2) . '/';
                }
            }
            $expense = '';
            for ($i = 1; $i <= 46; $i++) {
                if ($i == 46) {
                    $expense .= $this->format_float($this->input->post('expense' . $i), 2);
                } else {
                    $expense .= $this->format_float($this->input->post('expense' . $i), 2) . '/';
                }
            }

            $salary = $this->format_float($this->input->post('salary'));
            $total = $this->format_float($this->input->post('total'));
            $total_expense = $this->format_float($this->input->post('total_expense'));
            $grand_total = $this->format_float($this->input->post('grand_total'));
            $month_name = $this->getMonthNmae($s_date);

            $data_insert = [
                'slip_date' => $s_date,
                'm_id' => $id,
                'salary' => $salary,
                'in_come' => $in_come,
                'total' => $total,
                'expense' => $expense,
                'grand_expense' => $total_expense,
                'grand_total' => $grand_total,
                'staff_id' => $this->session->userdata('users')['m_id'],
                'slip_status' => 1,
                'created_at' => date('Y-m-d H:s:i'),
            ];

            $data_users = $this->users->get_users_id($id);


            if (isset($s_date)) {
                if ($data_users['cat_type'] == 0) {
                    $base_url = base_url('admin/slip-admin/' . $data_users['g_id'] . '/' . $data_users['cat_type'] . '?month=' . $s_date);
                } else if ($data_users['cat_type'] == 1) {
                    $base_url = base_url('admin/dashboard/888888?month=' . $s_date);
                } else if ($data_users['cat_type'] == 2) {
                    $base_url = base_url('admin/slip-admin/' . $data_users['g_id'] . '/' . $data_users['cat_type'] . '?month=' . $s_date);
                } else if ($data_users['cat_type'] == 3) {
                    $base_url = base_url('admin/dashboard/999999?month=' . $s_date);
                }
            } else {
                if ($data_users['cat_type'] == 0) {
                    $base_url = base_url('admin/slip-admin/' . $data_users['g_id'] . '/' . $data_users['cat_type']);
                } else if ($data_users['cat_type'] == 1) {
                    $base_url = base_url('admin/dashboard/888888');
                } else if ($data_users['cat_type'] == 2) {
                    $base_url = base_url('admin/slip-admin/' . $data_users['g_id'] . '/' . $data_users['cat_type']);
                } else if ($data_users['cat_type'] == 3) {
                    $base_url = base_url('admin/dashboard/999999');
                }
            }

            $result = $this->slip->add_slip($data_insert);
            if ($result == 'success') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'จัดการรายการเงินเดือนประจำเดือน ' . $month_name . ' สำเร็จ .');
                redirect($base_url);
            } else if ($result == 'false') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'จัดการรายการเงินเดือนประจำเดือน ' . $month_name . ' ไม่สำเร็จ .');
                redirect(base_url('admin/slip-add/' . $s_date . '/' . $id));
            }
        }
    }


    public function update($id = null)
    {
        if ($this->input->get()) {
            $month = $this->input->get('s_month');
            $cat_type = $this->input->get('cat_type');
            $s_date = $this->input->get('s_date');
            $m_id = $this->input->get('m_id');

            if ($cat_type == 1) {
                $type = '888888';
            } else if ($cat_type == 3) {
                $type = '999999';
            }

            $in_come = '';
            for ($i = 1; $i <= 6; $i++) {
                if ($i == 6) {
                    $in_come .= $this->format_float($this->input->get('in_come' . $i), 2);
                } else {
                    $in_come .= $this->format_float($this->input->get('in_come' . $i), 2) . '/';
                }
            }
            $expense = '';
            for ($i = 1; $i <= 46; $i++) {
                if ($i == 46) {
                    $expense .= $this->format_float($this->input->get('expense' . $i), 2);
                } else {
                    $expense .= $this->format_float($this->input->get('expense' . $i), 2) . '/';
                }
            }
            $salary = $this->format_float($this->input->get('salary'));
            $total = $this->format_float($this->input->get('total'));
            $total_expense = $this->format_float($this->input->get('total_expense'));
            $grand_total = $this->format_float($this->input->get('grand_total'));
            $month_name = $this->getMonthNmae($s_date);

            $data_insert = [
                'salary' => $salary,
                'in_come' => $in_come,
                'total' => $total,
                'expense' => $expense,
                'grand_expense' => $total_expense,
                'grand_total' => $grand_total,
                'staff_id' => $this->session->userdata('users')['m_id'],
                'slip_status' => 1,
            ];

            $data_users = $this->users->get_users_id($m_id);

            if (isset($month)) {
                if ($data_users['cat_type'] == 0) {
                    $base_url = base_url('admin/slip-admin/' . $data_users['g_id'] . '/' . $data_users['cat_type'] . '?month=' . $month);
                } else if ($data_users['cat_type'] == 1) {
                    $base_url = base_url('admin/dashboard/888888?month=' . $month);
                } else if ($data_users['cat_type'] == 2) {
                    $base_url = base_url('admin/slip-admin/' . $data_users['g_id'] . '/' . $data_users['cat_type'] . '?month=' . $month);
                } else if ($data_users['cat_type'] == 3) {
                    $base_url = base_url('admin/dashboard/999999?month=' . $month);
                }
            } else {
                if ($data_users['cat_type'] == 0) {
                    $base_url = base_url('admin/slip-admin/' . $data_users['g_id'] . '/' . $data_users['cat_type']);
                } else if ($data_users['cat_type'] == 1) {
                    $base_url = base_url('admin/dashboard/888888');
                } else if ($data_users['cat_type'] == 2) {
                    $base_url = base_url('admin/slip-admin/' . $data_users['g_id'] . '/' . $data_users['cat_type']);
                } else if ($data_users['cat_type'] == 3) {
                    $base_url = base_url('admin/dashboard/999999');
                }
            }

            $result = $this->slip->update_slip($id, $data_insert);
            if ($result == 'success') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'จัดการรายการเงินเดือนประจำเดือน ' . $month_name . ' สำเร็จ .');
                redirect($base_url);
            } else if ($result == 'false') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'จัดการรายการเงินเดือนประจำเดือน ' . $month_name . ' ไม่สำเร็จ .');
                redirect(base_url('admin/slip-update/' . $s_date . '/' . $id));
            }
        }
    }

    public function print_slip($month = null, $id = null)
    {
        if ($month && $id) {
            $this->print_terminal($month, $id);
        } else {
            // admin_redirect($_SERVER['HTTP_REFERER'] ?? 'sales');
        }
    }

    public function user_slip($month = null, $id = null)
    {
        // if ($month && $id) {
        //     $this->print_terminal($month, $id);
        // } else {
        //     // admin_redirect($_SERVER['HTTP_REFERER'] ?? 'sales');
        // }
    }

    function print_terminal($month, $id)
    {
        $slip_admin = $this->slip->slip_m_id($month, $id);
        $month_name = $this->getMonthNmae($month);
        $in_come = explode("/", $slip_admin['in_come']);
        $expense = explode("/", $slip_admin['expense']);

        $ex_id = [];
        foreach ($expense as $row) {
            if ($row != 0) {
                $ex_id[] = $row;
            }
        }
        $count_exp = COUNT($ex_id);
        $slip_id = urldecode($month);
        $slip_id = str_replace('#', '/', $slip_id);
        require_once(APPPATH . 'helpers/tcpdf1/tcpdf.php');

        $hha = 110;
        if ($count_exp != 0) {
            $xpls = 7 * $count_exp;
        } else {
            $xpls = 0;
        }

        $py = 0;
        if ($count_exp != 0) {
            $py = 3 * $count_exp;
        } else {
            $py = 0;
        }

        if (isset($in_come[5])) {
            $in_come[5] = $in_come[5];
        } else {
            $in_come[5] = 0;
        }

        if (isset($expense[43])) {
            $expense[43] =  $expense[43];
        } else {
            $expense[43] = 0;
        }
        if (isset($expense[44])) {
            $expense[44] =  $expense[44];
        } else {
            $expense[44] = 0;
        }
        if (isset($expense[45])) {
            $expense[45] =  $expense[45];
        } else {
            $expense[45] = 0;
        }
        if (isset($expense[46])) {
            $expense[46] =  $expense[46];
        } else {
            $expense[46] = 0;
        }
        $xha = 115 + $xpls;
        $img_file = base_url('./assets/img/ICON-02.png');
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array($hha, $xha), true, 'UTF-8', false);
        // กำหนดฟอนท์, ฟอนท์ freeserif รองรับภาษาไทย

        $pdf->SetFont('freeserif', 'B', 12, '', true);
        $pdf->setPrintHeader(false);
        $pdf->SetMargins(2, 3, 2, true);
        $pdf->SetTitle('พิมพ์รายงาน');
        $pdf->AddPage();
        // set background image
        $pdf->SetAlpha(0.4);
        $pdf->Image($img_file, $x = 25, $y = 30 + $py, $w = 60, $h = 60, $type = '', $link = '', $align = 'c', $resize = false, $dpi = 300, $palign = '50', $ismask = false, $imgmask = false, $border = 0);
        $pdf->SetAlpha(1);
        $pdf->setPageMark();
        $pdf->setPrintFooter(false);
        $pdf->SetFooterMargin(0);
        // get the current page break margin
        $bMargin = $pdf->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $pdf->getAutoPageBreak();
        // disable auto-page-break
        $pdf->SetAutoPageBreak(true, 0);
        $pdf->Cell(106, 5, 'เทศบาลนครลำปาง', '', 1, 'C', 0, '', 1);
        $pdf->Cell(106, 5, 'รายการเงินเดือนประจำเดือน ' . $month_name, '', 1, 'C', 0, '', 1);

        $pdf->SetFont('freeserif', '', 10, '', true);
        $html = '<br><br><table border="1"  cellpadding="60%">
                    <tr>
                        <td colspan="2" width="50%" style="font-size:9px"> ชื่อ-สกุล : <br>&nbsp;&nbsp;' . $slip_admin['m_title'] . $slip_admin['m_fname'] . ' ' . $slip_admin['m_lname'] . '</td>
                        <td colspan="2" width="50%" style="font-size:9px"> ตำแหน่ง : ' . $slip_admin['m_position'] . '<br> สังกัด : ' . $slip_admin['sub_name'] . '</td>
                    </tr>
                    <tr>
                        <td width="10%" colspan="4" width="100%" style="text-align:center" align="center" valign="middle"><b>รายการรับ</b></td>
                    </tr>
                    <tr>
                        <td width="50%" >เงินเดือน</td>
                        <td width="40%" style="text-align:right">' . number_format($slip_admin['salary'], 2) . '</td>
                        <td width="10%" style="text-align:center">บาท</td>
                    </tr>
                    <tr>
                        <td width="50%">ปจต</td>
                        <td width="40%" style="text-align:right">' . number_format($in_come[0], 2) . '</td>
                        <td width="10%" style="text-align:center">บาท</td>
                    </tr>
                    <tr>
                        <td width="50%">เงินเพิ่มต่างๆ</td>
                        <td width="40%" style="text-align:right">' . number_format($in_come[1], 2) . '</td>
                        <td width="10%" style="text-align:center">บาท</td>
                    </tr>
                    <tr>
                        <td width="50%">ค่าตอบแทนพิเศษ</td>
                        <td width="40%" style="text-align:right">' . number_format($in_come[2], 2) . '</td>
                        <td width="10%" style="text-align:center">บาท</td>
                    </tr>
                    <tr>
                        <td width="50%">ค่าครองชีพชั่วคราว</td>
                        <td width="40%" style="text-align:right">' . number_format($in_come[3], 2) . '</td>
                        <td width="10%" style="text-align:center">บาท</td>
                    </tr>
                    <tr>
                        <td width="50%">เงินเพิ่มค่าปรับวุฒิ</td>
                        <td width="40%" style="text-align:right">' . number_format($in_come[4], 2) . '</td>
                        <td width="10%" style="text-align:center">บาท</td>
                    </tr>
                    <tr>
                        <td width="50%">อื่นๆ</td>
                        <td width="40%" style="text-align:right">' .  number_format($in_come[5], 2) . '</td>
                        <td width="10%" style="text-align:center">บาท</td>
                    </tr>
                    <tr>
                        <td width="50%" style="text-align:right;"><b>รวมรับ</b></td>
                        <td  width="40%"style="text-align:right"><b>' . number_format($slip_admin['total'], 2) . '</b></td>
                        <td width="10%" style="text-align:center"><b>บาท</b></td>
                    </tr>';
        $html .= '<tr>
                    <td width="10%" colspan="4" width="100%" style="text-align:center" align="center" valign="middle"><b>รายการหัก</b></td>
                </tr>';
        if ($expense[0] != 0) {
            // หัก 1
            $html .= '<tr >
                    <td width="50%">ภาษี ณ ที่จ่าย</td>
                    <td width="40%" style="text-align:right">' . number_format($expense[0], 2) . '</td>
                    <td width="10%" style="text-align:center">บาท</td>
                </tr>';
        }
        if ($expense[1] != 0) {
            // หัก 2
            $html .= '<tr>
                    <td width="50%">ประกันสังคม</td>
                    <td width="40%"style="text-align:right">' . number_format($expense[1], 2) . '</td>
                    <td width="10%" style="text-align:center">บาท</td>
                </tr>';
        }
        if ($expense[2] != 0) {
            // หัก 3
            $html .= '<tr>
                    <td width="50%">กบข.</td>
                    <td width="40%"style="text-align:right">' . number_format($expense[2], 2) . '</td>
                    <td width="10%" style="text-align:center">บาท</td>
                </tr>';
        }
        if ($expense[3] != 0) {
            // หัก 4
            $html .= '<tr>
                <td width="50%">กสจ.</td>
                <td width="40%"style="text-align:right">' . number_format($expense[3], 2) . '</td>
                <td width="10%" style="text-align:center">บาท</td>
            </tr>';
        }
        if ($expense[4] != 0) {
            // หัก 5
            $html .= '<tr>
            <td width="50%">การฌาปนกิจสงเคราะห์ ปภ.</td>
            <td width="40%"style="text-align:right">' . number_format($expense[4], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[5] != 0) {
            // หัก 6
            $html .= '<tr>
                <td width="50%">ธนาคารออนสิน สาขาบ๊กซีลำปาง</td>
                <td width="40%"style="text-align:right">' . number_format($expense[5], 2) . '</td>
                <td width="10%" style="text-align:center">บาท</td>
            </tr>';
        }
        if ($expense[6] != 0) {
            // หัก 7
            $html .= '<tr>
            <td width="50%">การฌาปนกิจสงเคราะห์ข้าราชการและบุคลากรท้องถิ่น (ก.ฌ.)</td>
            <td width="40%"style="text-align:right">' . number_format($expense[6], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[7] != 0) {
            // หัก 8
            $html .= '<tr>
                <td width="50%">ธนาคารอาคารสงเคราะห์ สาขาลำปาง</td>
                <td width="40%"style="text-align:right">' . number_format($expense[7], 2) . '</td>
                <td width="10%" style="text-align:center">บาท</td>
            </tr>';
        }
        if ($expense[8] != 0) {
            // หัก 9
            $html .= '<tr>
            <td width="50%">ธนาคารออนสิน สาขาลำปาง</td>
            <td width="40%"style="text-align:right">' . number_format($expense[8], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[9] != 0) {
            // หัก 10
            $html .= '<tr>
            <td width="50%">ธนาคารออนสิน สาขาสบตุ๋ย</td>
            <td width="40%"style="text-align:right">' . number_format($expense[9], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[10] != 0) {
            // หัก 11
            $html .= '<tr>
            <td width="50%">ธนาคารกรุงไทย จำกัด (มหาชน) สาขาลำปาง</td>
            <td width="40%"style="text-align:right">' . number_format($expense[10], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[11] != 0) {
            // หัก 12
            $html .= '<tr>
            <td width="50%">ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร สาขาลำปาง</td>
            <td width="40%"style="text-align:right">' . number_format($expense[11], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[12] != 0) {
            // หัก 13
            $html .= '<tr>
         <td width="50%">กองทุนบำเหน็จบำนาญข้าราชการ</td>
         <td width="40%"style="text-align:right">' . number_format($expense[12], 2) . '</td>
         <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[13] != 0) {
            // หัก 14
            $html .= '<tr>
            <td width="50%">สหกรณ์ออมทรัพย์พนักงานเทศบาล จำกัด</td>
            <td width="40%"style="text-align:right">' . number_format($expense[13], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[14] != 0) {
            // หัก 15
            $html .= '<tr>
            <td width="50%">สหกรณ์ออมทรัพย์กรมโยธาธิการ 2529 จำกัด</td>
            <td width="40%"style="text-align:right">' . number_format($expense[14], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[15] != 0) {
            // หัก 16
            $html .= '<tr>
            <td width="50%">สหกรณ์ออมทรัพย์ รพช. จำกัด</td>
            <td width="40%"style="text-align:right">' . number_format($expense[15], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[16] != 0) {
            // หัก 17
            $html .= '<tr>
            <td width="50%">กองทุนสวัสดิการ พนักงานและลูกจ้าง (กองคลัง)</td>
            <td width="40%"style="text-align:right">' . number_format($expense[16], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[17] != 0) {
            // หัก 18
            $html .= '<tr>
            <td width="50%">ช.พ.ค.ลำปาง</td>
            <td width="40%"style="text-align:right">' . number_format($expense[17], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[18] != 0) {
            // หัก 19
            $html .= '<tr>
            <td width="50%">ช.พ.ส.</td>
            <td width="40%"style="text-align:right">' . number_format($expense[18], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[19] != 0) {
            // หัก 20
            $html .= '<tr>
            <td width="50%">สมาคม ชค.ลป.</td>
            <td width="40%"style="text-align:right">' . number_format($expense[19], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[20] != 0) {
            // หัก 21
            $html .= '<tr>
            <td width="50%">สหกรณ์ออมทรัพย์พนักงานครูลำปาง</td>
            <td width="40%"style="text-align:right">' . number_format($expense[20], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[21] != 0) {
            // หัก 22
            $html .= '<tr>
            <td width="50%">เทศบาลตำบลวังเหนือ</td>
            <td width="40%"style="text-align:right">' . number_format($expense[21], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[22] != 0) {
            // หัก 23
            $html .= '<tr>
            <td width="50%">เงินสวัสดิการสำนักการศึกษา</td>
            <td width="40%"style="text-align:right">' . number_format($expense[22], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[23] != 0) {
            // หัก 24
            $html .= '<tr>
            <td width="50%">เงินกองทุนสวัสดิการกองการศึกษา</td>
            <td width="40%"style="text-align:right">' . number_format($expense[23], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[24] != 0) {
            // หัก 25
            $html .= '<tr>
            <td width="50%">สวัสดิการสำนักการศึกษา</td>
            <td width="40%"style="text-align:right">' . number_format($expense[24], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[25] != 0) {
            // หัก 26
            $html .= '<tr>
            <td width="50%">ธนาคารออนสิน สาขาเซ็นทรัลลำปาง</td>
            <td width="40%"style="text-align:right">' . number_format($expense[25], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[26] != 0) {
            // หัก 27
            $html .= '<tr>
            <td width="50%">ธนาคารอิสลาม</td>
            <td width="40%"style="text-align:right">' . number_format($expense[26], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[27] != 0) {
            // หัก 28
            $html .= '<tr>
            <td width="50%">กลุ่มพัฒนาอาชีพ</td>
            <td width="40%"style="text-align:right">' . number_format($expense[27], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[28] != 0) {
            // หัก 29
            $html .= '<tr>
            <td width="50%">เงินสะสม</td>
            <td width="40%"style="text-align:right">' . number_format($expense[28], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[29] != 0) {
            // หัก 30
            $html .= '<tr>
            <td width="50%">สหกรณ์ออมทรัพย์องค์กรปกครองส่วนท้องถิ่น จำกัด</td>
            <td width="40%"style="text-align:right">' . number_format($expense[29], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[30] != 0) {
            // หัก 31
            $html .= '<tr>
            <td width="50%">สหกรณ์ออมทรัพย์พนักงานองค์การปกครองส่วนท้องถิ่น จังหวัดลำปาง</td>
            <td width="40%"style="text-align:right">' . number_format($expense[30], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[31] != 0) {
            // หัก 32
            $html .= '<tr>
            <td width="50%">กองทุนเงินให้กู้ยืมเพื่อการศึกษา</td>
            <td width="40%"style="text-align:right">' . number_format($expense[31], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[32] != 0) {
            // หัก 33
            $html .= '<tr>
            <td width="50%">สวัสดิการครูโรงเรียนเทศบาล 1</td>
            <td width="40%"style="text-align:right">' . number_format($expense[32], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[33] != 0) {
            // หัก 34
            $html .= '<tr>
            <td width="50%">สวัสดิการครูโรงเรียนเทศบาล 4</td>
            <td width="40%"style="text-align:right">' . number_format($expense[33], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[34] != 0) {
            // หัก 35
            $html .= '<tr>
            <td width="50%">เทศบาลนครลำปาง</td>
            <td width="40%"style="text-align:right">' . number_format($expense[34], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[35] != 0) {
            // หัก 36
            $html .= '<tr>
            <td width="50%">สำนักงานส่งเสริมการปกครองท้องถิ่นจังหวุดลำปาง</td>
            <td width="40%"style="text-align:right">' . number_format($expense[35], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[36] != 0) {
            // หัก 37
            $html .= '<tr>
            <td width="50%">รร.เทศบาล 6 (กองทุนสวัสดิการ)</td>
            <td width="40%"style="text-align:right">' . number_format($expense[36], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[37] != 0) {
            // หัก 38
            $html .= '<tr>
            <td width="50%">หกรณ์ออมทรัพย์ข้าราชการองค์การบริหารส่วนจังหวัด จำกัด</td>
            <td width="40%"style="text-align:right">' . number_format($expense[37], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[38] != 0) {
            // หัก 39
            $html .= '<tr>
            <td width="50%">สหกรณ์ออมทรัพย์ครูกรมสามัญศึกษา จ.เลย</td>
            <td width="40%"style="text-align:right">' . number_format($expense[38], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[39] != 0) {
            // หัก 40
            $html .= '<tr>
            <td width="50%">สหกรณ์ออมทรัพย์ครูระยอง</td>
            <td width="40%"style="text-align:right">' . number_format($expense[39], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[40] != 0) {
            // หัก 41
            $html .= '<tr>
            <td width="50%">สหกรณ์ออมทรัพย์ข้าราชการกระทรวงศึกษาธิการเชียงใหม่ จำกัดด</td>
            <td width="40%"style="text-align:right">' . number_format($expense[40], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[41] != 0) {
            // หัก 42
            $html .= '<tr>
            <td width="50%">ฌาปนกิจสงเคราะห์ข้าราชการส่วนท้องถิ่นเชียงใหม่</td>
            <td width="40%"style="text-align:right">' . number_format($expense[41], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[42] != 0) {
            // หัก 43
            $html .= '<tr>
            <td width="50%">สหกรณ์ออมทรัพย์กระทรวงแรงงาน จำกัด</td>
            <td width="40%"style="text-align:right">' . number_format($expense[42], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[43] != 0) {
            // หัก 43
            $html .= '<tr>
            <td width="50%">ชมรม บน.</td>
            <td width="40%"style="text-align:right">' . number_format($expense[43], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[44] != 0) {
            // หัก 43
            $html .= '<tr>
            <td width="50%">เบ็ดเตล็ด</td>
            <td width="40%"style="text-align:right">' . number_format($expense[44], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if ($expense[45] != 0) {
            // หัก 43
            $html .= '<tr>
            <td width="50%">อื่นๆ</td>
            <td width="40%"style="text-align:right">' . number_format($expense[45], 2) . '</td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }
        if (
            $expense[0] == 0 && $expense[1] == 0 && $expense[2] == 0 && $expense[3] == 0 && $expense[4] == 0
            && $expense[5] == 0 && $expense[6] == 0 && $expense[7] == 0 && $expense[8] == 0 && $expense[9] == 0
            && $expense[10] == 0 && $expense[11] == 0 && $expense[12] == 0 && $expense[13] == 0 && $expense[14] == 0
            && $expense[15] == 0 && $expense[16] == 0 && $expense[17] == 0 && $expense[18] == 0 && $expense[19] == 0
            && $expense[20] == 0 && $expense[21] == 0 && $expense[22] == 0 && $expense[23] == 0 && $expense[24] == 0
            && $expense[25] == 0 && $expense[26] == 0 && $expense[27] == 0 && $expense[28] == 0 && $expense[29] == 0
            && $expense[30] == 0 && $expense[31] == 0 && $expense[32] == 0 && $expense[33] == 0 && $expense[34] == 0
            && $expense[35] == 0 && $expense[36] == 0 && $expense[37] == 0 && $expense[38] == 0 && $expense[39] == 0
            && $expense[40] == 0 && $expense[41] == 0 && $expense[42] == 0 && $expense[43] == 0 && $expense[44] == 0
            && $expense[45] == 0 && $expense[46] == 0
        ) {
            $html .= '<tr>
            <td width="50%"><b>-</b></td>
            <td width="40%"style="text-align:right"> 0.00 </td>
            <td width="10%" style="text-align:center">บาท</td>
        </tr>';
        }

        $html .= '<tr>
            <td width="50%" style="text-align:right;"><b>รวมหัก</b></td>
            <td width="40%" style="text-align:right"><b>' . number_format($slip_admin['grand_expense'], 2) . '</b></td>
            <td width="10%" style="text-align:center"><b>บาท</b></td>
        </tr>';
        $html .= '<tr>
            <td width="50%" style="text-align:right;"><b>คงรับจริงจำนวน</b></td>
            <td width="40%" style="text-align:right"><b>' . number_format($slip_admin['grand_total'], 2) . '</b></td>
            <td width="10%" style="text-align:center"><b>บาท</b></td>
        </tr>';
        $html .= "</table>";
        $pdf->writeHTML($html);
        $pdf->lastPage();
        $pdf->Output('tutorial.pdf', 'I');
    }
}
