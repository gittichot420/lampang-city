<?php
defined('BASEPATH') or exit('No direct script access allowed');

class subGroup extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (empty($this->session->userdata('users'))) {
            redirect(base_url());
        }
        $this->load->model('admin/group_model', 'group');
        $this->load->model('admin/sub_group_model', 'sub_group');
    }

    public function index($id = null)
    {
        $this->data['page'] = 'list';
        $this->data['get_group'] = $this->group->get_group();
        $this->data['groupName'] = $this->group->groupName($id);
        $this->data['subGroupId'] = $this->sub_group->get_subGroup($id);

        $this->load->view('admin/theme/header', $this->data);
        $this->load->view('admin/sub_group/index', $this->data);
        $this->load->view('admin/sub_group/modal');
        $this->load->view('admin/theme/footer');
    }

    public function create()
    {
        if ($this->input->post()) {
            $g_id = $this->input->post('g_id');
            $sub_name = $this->input->post('sub_group');

            $sub_group = $this->sub_group->check_subGroup($sub_name);
            if ($sub_group == NULL) {
                $data_insert = [
                    'g_id' => $g_id,
                    'sub_name' => $sub_name
                ];
                $result = $this->sub_group->add_subGroup($data_insert);
                if ($result == 'success') {
                    $this->session->set_flashdata('result', $result);
                    $this->session->set_flashdata('message', 'เพิ่มข้อมูลสังกัดสำเร็จ.');
                    redirect(base_url('admin/group/list/' . $g_id));
                } else if ($result == 'false') {
                    $this->session->set_flashdata('result', $result);
                    $this->session->set_flashdata('message', 'เพิ่มข้อมูลสังกัดไม่สำเร็จ.');
                    redirect(base_url('admin/group/list/' . $g_id));
                }
            } else {
                $this->session->set_flashdata('result', 'duplicate');
                $this->session->set_flashdata('message', 'ข้อมูลสังกัดนี้มีอยู่ในระบบแล้ว.');
                redirect(base_url('admin/group/list/' . $g_id));
            }
        } else {
            redirect(base_url('admin/group'));
        }
    }

    public function update($id = NULL)
    {
        if ($this->input->get()) {
            $sub_id = $this->input->get('sub_id');
            $g_id = $this->input->get('g_id');
            $sub_name = $this->input->get('sub_name');
            $data_insert = [
                'g_id' => $g_id,
                'sub_name' => $sub_name,
            ];
            $sub_group = $this->sub_group->check_subGroup($sub_name);
            if ($sub_group == NULL) {
                $result = $this->sub_group->update_subGroup($sub_id, $data_insert);
                if ($result == 'success') {
                    $this->session->set_flashdata('result', $result);
                    $this->session->set_flashdata('message', 'แก้ไขข้อมูลสังกัดสำเร็จ.');
                    redirect(base_url('admin/group/list/' . $id));
                } else if ($result == 'false') {
                    $this->session->set_flashdata('result', $result);
                    $this->session->set_flashdata('message', 'แก้ไขข้อมูลสังกัดไม่สำเร็จ.');
                    redirect(base_url('admin/group/list/' . $id));
                }
            } else if ($sub_group['sub_id'] == $sub_id) {
                $result = $this->sub_group->update_subGroup($sub_id, $data_insert);
                if ($result == 'success') {
                    $this->session->set_flashdata('result', $result);
                    $this->session->set_flashdata('message', 'แก้ไขข้อมูลสังกัดสำเร็จ.');
                    redirect(base_url('admin/group/list/' . $id));
                } else if ($result == 'false') {
                    $this->session->set_flashdata('result', $result);
                    $this->session->set_flashdata('message', 'แก้ไขข้อมูลสังกัดไม่สำเร็จ.');
                    redirect(base_url('admin/group/list/' . $id));
                }
            } else {
                $this->session->set_flashdata('result', 'duplicate');
                $this->session->set_flashdata('message', 'ข้อมูลสังกัดนี้มีอยู่ในระบบแล้ว.');
                redirect(base_url('admin/group/list/' . $id));
            }
        } else {
            redirect(base_url('admin/group'));
        }
    }

    public function delete($id = NULL)
    {
        if ($id != null) {
            $sub_id = $this->input->get('sub_id');
            $result = $this->sub_group->del_subGroup($sub_id);
            if ($result == 'success') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'ลบข้อมูลสังกัดสำเร็จ.');
                redirect(base_url('admin/group/list/' . $id));
            } else if ($result == 'false') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'ลบข้อมูลสังกัดไม่สำเร็จ.');
                redirect(base_url('admin/group/list/' . $id));
            }
        } else {
            redirect(base_url('admin/group'));
        }
    }
}