<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (empty($this->session->userdata('users'))) {
            redirect(base_url());
        }
        $this->load->model('admin/assign_model', 'assign');
        $this->load->model('admin/slip_model', 'slip');
    }

    public function index()
    {
        $month = $this->input->get('month');
        if (empty($month)) {
            $month = date('Y-m');
        }
        $mem_id = $this->session->userdata('users')['m_id'];
        $assign = $this->assign->assign_id($mem_id);
        if ($assign != null) {
            $assign_id = explode("/", $assign['as_sub_id']);
            $group_assign = $this->assign->group_assign($assign_id);
            $cat_type = [];
            $group_id = [];
            foreach ($group_assign as $value) {
                if ($value['g_id'] == 999999) {
                    $cat_type[] = 3;
                } else if ($value['g_id'] == 888888) {
                    $cat_type[] = 1;
                } else {
                    $group_id[] = $value['g_id'];
                }
            }
        } else {
            $assign_id = [];
            $group_assign = [];
            $cat_type = [];
            $group_id = [];

        }

        $memIdALl = [];
        if ($cat_type != null) {
            //   หา ผู้บริหาร && ข้าราชการบำนาญ
            $type_user = $this->assign->get_user_type($cat_type);
            for ($i = 0; $i < count($type_user); $i++) {
                $memIdALl[] = $type_user[$i]['m_id'];
            }
        } else {
            $type_user = 0;
        }

        if ($group_id != null) {
            $userGroupType = $this->assign->userGroupType($group_id);
            for ($i = 0; $i < count($userGroupType); $i++) {
                $memIdALl[] = $userGroupType[$i]['m_id'];
            }
        }

        if ($memIdALl != null) {
            $memSuccess = $this->slip->slipMemArr($month, $memIdALl);
            $sumSuccess = count($memSuccess);
        } else {
            $sumSuccess = 0;
        }

        $sumAll = count($memIdALl);

        $this->data['month'] = $month;
        $this->data['sumSuccess'] = $sumSuccess;
        $this->data['sumALl'] = $sumAll;
        $this->data['group_assign'] = $group_assign;

        $this->load->view('admin/theme/header');
        $this->load->view('admin/index', $this->data);
        $this->load->view('admin/theme/footer');


    }
}