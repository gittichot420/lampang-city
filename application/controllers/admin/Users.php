<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (empty($this->session->userdata('users'))) {
            redirect(base_url());
        }
        $this->load->model('admin/users_model', 'users');
        $this->load->model('admin/group_model', 'group');
        $this->load->model('admin/category_model', 'category');
        $this->load->model('admin/sub_group_model', 'sub_group');
    }

    public function password_hash($password)
    {
        return md5($password);
    }

    public function index()
    {
        $session = $this->session->userdata('users')['status'];
        $this->data['get_users'] = $this->users->get_users($session);

        $this->load->view('admin/theme/header');
        $this->load->view('admin/users/index', $this->data);
        $this->load->view('admin/theme/footer');
    }

    public function add()
    {
        if ($this->input->post()) {
            $title = $this->input->post('title');
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $card_id = $this->input->post('card_id');
            $phone = $this->input->post('phone');
            if ($this->input->post('sub_group') != null) {
                $sub_group = $this->input->post('sub_group');
            } else {
                $sub_group = null;
            }
            $category = $this->input->post('category');
            $position = $this->input->post('position');
            $salary = $this->input->post('salary');
            $created_at = date('Y-m-d H:i:s');
            $password = $this->input->post('password');
            $status = $this->input->post('status');

            if ($status == 2) {
                $data_insert = [
                    'm_title' => $title,
                    'm_fname' => $first_name,
                    'm_lname' => $last_name,
                    'm_card_id' => $card_id,
                    'm_password' => '',
                    'm_phone' => $phone,
                    'm_position' => $position,
                    'm_salary' => $salary,
                    'sub_id' => $sub_group,
                    'm_type' => $category,
                    'status' => $status,
                    'created_at' => $created_at,
                ];
            } else {
                if (!empty($password)) {
                    $pass_hash = $this->password_hash($password);
                    $data_insert = [
                        'm_title' => $title,
                        'm_fname' => $first_name,
                        'm_lname' => $last_name,
                        'm_card_id' => $card_id,
                        'm_password' => $pass_hash,
                        'm_phone' => $phone,
                        'm_position' => $position,
                        'm_salary' => $salary,
                        'sub_id' => $sub_group,
                        'm_type' => $category,
                        'status' => $status,
                        'created_at' => $created_at,
                    ];
                } else {
                    $this->session->set_flashdata('result', 'false');
                    $this->session->set_flashdata('message', 'โปรดระบุข้อมูลรหัสผ่าน.');
                    redirect(base_url('admin/users/add'));
                }
            }

            $check_card_id = $this->users->check_card($card_id);
            if ($check_card_id == NULL) {
                $result = $this->users->add_users($data_insert);
                if ($result == 'success') {
                    $this->session->set_flashdata('result', $result);
                    $this->session->set_flashdata('message', 'เพิ่มข้อมูลสมาชิกสำเร็จ.');
                    redirect(base_url('admin/users'));
                } else if ($result == 'false') {
                    $this->session->set_flashdata('result', $result);
                    $this->session->set_flashdata('message', 'เพิ่มข้อมูลสมาชิกไม่สำเร็จ.');
                    redirect(base_url('admin/users'));
                }
            } else {
                $this->session->set_flashdata('result', 'duplicate');
                $this->session->set_flashdata('message', 'เลขบัตรประจำตัวประชาชนนี้มีอยู่ในระบบแล้ว.');
                redirect(base_url('admin/users/add'));
            }
        } else {
            $this->data['page'] = 'users';
            $this->data['get_group'] = $this->group->get_group();
            $this->data['get_subGroup'] = $this->sub_group->get_Group();
            $this->data['get_category'] = $this->category->get_category();

            $this->load->view('admin/theme/header', $this->data);
            $this->load->view('admin/users/add', $this->data);
            $this->load->view('admin/theme/footer');
        }
    }

    public function edit($id = null)
    {
        if ($this->input->get()) {
            $title = $this->input->get('title');
            $first_name = $this->input->get('first_name');
            $last_name = $this->input->get('last_name');
            $card_id = $this->input->get('card_id');
            $phone = $this->input->get('phone');
            if ($this->input->get('sub_group') != null) {
                $sub_group = $this->input->get('sub_group');
            } else {
                $sub_group = null;
            }
            $category = $this->input->get('category');
            $position = $this->input->get('position');
            $salary = $this->input->get('salary');
            $update_at = date('Y-m-d H:i:s');
            $password = $this->input->get('password');
            $status = $this->input->get('status');

            $check_card_id = $this->users->check_card($card_id);
            if ($check_card_id == null || $check_card_id['m_id'] == $id) {
                if ($status == 2) {
                    // members
                    $data_insert = [
                        'm_title' => $title,
                        'm_fname' => $first_name,
                        'm_lname' => $last_name,
                        'm_card_id' => $card_id,
                        'm_password' => '',
                        'm_phone' => $phone,
                        'm_position' => $position,
                        'm_salary' => $salary,
                        'sub_id' => $sub_group,
                        'm_type' => $category,
                        'status' => $status,
                        'update_at' => $update_at,
                    ];
                } else {
                    // Super Admin // Admin
                    $users_id = $this->users->get_users_id($id);
                    if (!empty($password)) {
                        $pass_hash = $this->password_hash($password);
                        $data_insert = [
                            'm_title' => $title,
                            'm_fname' => $first_name,
                            'm_lname' => $last_name,
                            'm_card_id' => $card_id,
                            'm_password' => $pass_hash,
                            'm_phone' => $phone,
                            'm_position' => $position,
                            'm_salary' => $salary,
                            'sub_id' => $sub_group,
                            'm_type' => $category,
                            'status' => $status,
                            'update_at' => $update_at,
                        ];
                    } else if ($users_id['m_password'] != null) {
                        $data_insert = [
                            'm_title' => $title,
                            'm_fname' => $first_name,
                            'm_lname' => $last_name,
                            'm_card_id' => $card_id,
                            'm_password' => $users_id['m_password'],
                            'm_phone' => $phone,
                            'm_position' => $position,
                            'm_salary' => $salary,
                            'sub_id' => $sub_group,
                            'm_type' => $category,
                            'status' => $status,
                            'update_at' => $update_at,
                        ];
                    } else {
                        $this->session->set_flashdata('result', 'false');
                        $this->session->set_flashdata('message', 'โปรดระบุข้อมูลรหัสผ่าน.');
                        redirect(base_url('admin/users/edit/' . $id));
                    }
                }
            } else {
                $this->session->set_flashdata('result', 'duplicate');
                $this->session->set_flashdata('message', 'เลขบัตรประจำตัวประชาชนนี้มีอยู่ในระบบแล้ว.');
                redirect(base_url('admin/group/list/' . $id));
            }
            $result = $this->users->update_users($id, $data_insert);
            if ($result == 'success') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'แก้ไขข้อมูลสมาชิกสำเร็จ.');
                redirect(base_url('admin/users'));
            } else if ($result == 'false') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'แก้ไขข้อมูลสมาชิกไม่สำเร็จ.');
                redirect(base_url('admin/users/edit') . $id);
            }
        } else {
            $this->data['page'] = 'users';
            $this->data['users_id'] = $this->users->get_users_id($id);
            $this->data['get_group'] = $this->group->get_group();
            $this->data['get_subGroup'] = $this->sub_group->get_Group();
            $this->data['get_category'] = $this->category->get_category();
            $this->load->view('admin/theme/header', $this->data);
            $this->load->view('admin/users/edit', $this->data);
            $this->load->view('admin/theme/footer');
        }
    }

    public function del($id = null)
    {
        if ($id != null) {
            $result = $this->users->del_users($id);
            if ($result == 'success') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'ลบข้อมูลผู้ใช้งานสำเร็จ.');
                redirect(base_url('admin/users'));
            } else if ($result == 'false') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'ลบข้อมูลผู้ใช้งานไม่สำเร็จ.');
                redirect(base_url('admin/users'));
            }
        } else {
            redirect(base_url('admin/users'));
        }
    }

    public function reset_data()
    {
        if ($this->session->userdata('users')['m_password'] == 'Superadmin1234') {
            $this->users->resetSalary();
            $this->users->delAllData();
            redirect(base_url('admin'), 'refresh');
        } else {
            redirect(base_url('admin'));
        }
    }
}
