<?php

defined('BASEPATH') or exit('No direct script access allowed');

class auth_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function login($card_id = null)
    {
        $where = ['m_card_id' => $card_id];
        return $this->db->join('sub_groups', 'members.sub_id = sub_groups.sub_id', 'left')->join('groups', 'groups.g_id = sub_groups.g_id', 'left')->join('category', 'category.cat_id = members.m_type', 'left')->where($where)->get('members')->row_array();
    }

    public function login_user($card_id = null, $ps_hash = null)
    {
        $where = ['u_user' => $card_id, 'u_pass' => $ps_hash, 'u_status' => 1];
        return $this->db->where($where)->get('users')->row_array();
    }

    public function register($data_insert = null)
    {
        $result = $this->db->insert('members', $data_insert);

        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }

    public function regis_users($data_insert = null)
    {
        $result = $this->db->insert('users', $data_insert);

        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }


    public function check_user($username = null)
    {
        return $this->db->where('u_username', $username)->get('users')->row_array();
    }

    public function get_user()
    {
        return $this->db->limit(1)->get('members')->result_array();
    }

    public function get_regisUser($iDcard)
    {
        return $this->db->where('u_user', $iDcard)->get('users')->result_array();
    }


    public function check_forgot($idCard, $email)
    {
        $where = ['u_user' => $idCard, 'u_email' => $email];
        return $this->db->where($where)->get('users')->row_array();
    }


    public function reset_password($_data, $id)
    {
        $result = $this->db->where('u_id', $id)->update('users', $_data);
        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }
}
