<?php

defined('BASEPATH') or exit('No direct script access allowed');

class category_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_category()
    {
        return $this->db->order_by('cat_type', 'ASC')->order_by('cat_name', 'ASC')->get('category')->result_array();
    }

    public function category_check($cat_name = null)
    {
        return $this->db->where('cat_name', $cat_name)->get('category')->row_array();
    }

    public function add_category($data_insert = null)
    {
        $result = $this->db->insert('category', $data_insert);

        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }

    public function update_category($id = null, $data_insert = null)
    {
        $result = $this->db->where('cat_id', $id)->update('category', $data_insert);

        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }

    public function del_category($id = null)
    {
        $result = $this->db->where('cat_id', $id)->delete('category');

        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }
}