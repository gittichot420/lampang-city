<?php

defined('BASEPATH') or exit('No direct script access allowed');

class group_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_group()
    {
        return $this->db->order_by('g_name', 'ASC')->group_by('groups.g_id')->where('groups.g_id !=', '999999')->where('groups.g_id !=', '888888')->join('sub_groups', 'sub_groups.g_id  = groups.g_id', 'left')->select('groups.g_id,
        groups.g_name,
        COUNT(tb_sub_groups.g_id) AS g_count')->get('groups')->result_array();
    }

    public function groupName($g_id = null)
    {
        return $this->db->where('g_id', $g_id)->get('groups')->row_array();
    }

    public function group_check($g_name = null)
    {
        return $this->db->where('g_name', $g_name)->get('groups')->row_array();
    }

    public function add_group($data_insert = null)
    {
        $result = $this->db->insert('groups', $data_insert);

        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }

    public function update_group($g_id = null, $data_insert = null)
    {
        $result = $this->db->where('g_id', $g_id)->update('groups', $data_insert);

        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }

    public function del_group($g_id = null)
    {
        $result = $this->db->where('g_id', $g_id)->delete('groups');

        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }
}