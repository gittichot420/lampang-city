<?php

defined('BASEPATH') or exit('No direct script access allowed');

class assign_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_admin()
    {
        return $this->db->order_by('members.status ASC')->order_by('members.m_fname ASC')
            ->where('members.status !=', '2')
            ->join('assign', 'assign.as_m_id = members.m_id')
            ->join('sub_groups', 'sub_groups.sub_id = members.sub_id', 'left')
            ->join('groups', 'groups.g_id = sub_groups.g_id', 'left')
            ->join('category', 'category.cat_id = members.m_type', 'left')
            ->select('members.m_id,members.m_title,members.m_fname,members.m_lname,members.m_position,
            members.m_type,category.cat_name,category.cat_type,assign.as_id,assign.as_sub_id')
            ->get('members')->result_array();
    }

    public function userGroupType($group = [])
    {
        return $this->db->order_by('members.m_fname ASC')
            ->where_in('groups.g_id', $group)
            ->where('category.cat_type !=', '1')
            ->where('category.cat_type !=', '3')
            ->join('sub_groups', 'sub_groups.sub_id = members.sub_id', 'left')
            ->join('groups', 'groups.g_id = sub_groups.g_id', 'left')
            ->join('category', 'category.cat_id = members.m_type', 'left')
            ->get('members')->result_array();
    }

    public function get_user_type($type = [])
    {
        return $this->db->order_by('members.m_fname ASC')
            ->where_in('cat_type', $type)
            ->join('sub_groups', 'sub_groups.sub_id = members.sub_id', 'left')
            ->join('groups', 'groups.g_id = sub_groups.g_id', 'left')
            ->join('category', 'category.cat_id = members.m_type', 'left')
            ->get('members')->result_array();
    }

    public function get_users_id($mem_id = null)
    {
        return $this->db->order_by('members.status ASC')->order_by('members.m_fname ASC')
            ->where('members.m_id', $mem_id)
            ->join('sub_groups', 'sub_groups.sub_id = members.sub_id', 'left')
            ->join('groups', 'groups.g_id = sub_groups.g_id', 'left')
            ->join('category', 'category.cat_id = members.m_type', 'left')
            ->select('members.m_id,members.m_title,members.m_fname,members.m_lname,members.m_card_id,
            members.m_phone,groups.g_id,groups.g_name,members.sub_id,sub_groups.sub_name,
            members.m_position,members.m_salary,members.m_type,category.cat_name,category.cat_type,members.status')
            ->get('members')->row_array();
    }

    public function assign_id($m_id = null)
    {
        return $this->db->where('as_m_id', $m_id)->get('assign')->row_array();
    }
    public function group_assign($g_id = [])
    {
        return $this->db->order_by('g_name', 'ASC')->where_in('g_id', $g_id)->get('groups')->result_array();
    }

    public function get_groupALL()
    {
        return $this->db->order_by('g_name', 'ASC')->group_by('groups.g_id')->join('sub_groups', 'sub_groups.g_id  = groups.g_id', 'left')->select('groups.g_id,
        groups.g_name,
        COUNT(tb_sub_groups.g_id) AS g_count')->get('groups')->result_array();
    }


    public function get_group($g_id = [])
    {
        return $this->db->order_by('g_name', 'ASC')
            ->where_not_in('g_id', $g_id)
            ->get('groups')->result_array();
    }
    public function get_member($m_id = [])
    {
        return $this->db->order_by('members.m_title ASC')
            ->order_by('members.m_fname ASC')
            ->where_not_in('members.m_id', $m_id)
            ->where('members.status !=', '2')
            ->get('members')->result_array();
    }

    public function get_as_admin()
    {
        return $this->db->order_by('members.m_title ASC')
            ->order_by('members.m_fname ASC')
            ->where('members.status !=', '2')
            ->get('members')->result_array();
    }

    public function g_name($g_id = null)
    {
        return $this->db->order_by('g_name ASC')->where_in('g_id', $g_id)->get('groups')->result_array();
    }

    public function add_assign($data_insert = null)
    {
        $result = $this->db->insert('assign', $data_insert);

        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }

    public function del_assign($as_id = null)
    {
        $result = $this->db->where('as_id', $as_id)->delete('assign');

        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }
}