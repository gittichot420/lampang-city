<?php

defined('BASEPATH') or exit('No direct script access allowed');

class sub_group_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_subGroup($g_id = null)
    {
        return $this->db->order_by('sub_name', 'ASC')->where('g_id', $g_id)->get('sub_groups')->result_array();
    }

    public function get_Group()
    {
        return $this->db->order_by('g_id', 'ASC')->order_by('sub_name', 'ASC')
            ->where('groups.g_id != 888888 AND groups.g_id != 999999')
            ->join('groups', 'groups.g_id = sub_groups.g_id')
            ->select('groups.g_id,groups.g_name,sub_groups.sub_id,sub_groups.sub_name')
            ->get('sub_groups')->result_array();
    }

    public function check_subGroup($sub_name = null)
    {
        return $this->db->where('sub_name', $sub_name)->get('sub_groups')->row_array();
    }

    public function add_subGroup($data_insert = null)
    {
        $result = $this->db->insert('sub_groups', $data_insert);

        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }

    public function update_subGroup($sub_id = null, $data_insert = null)
    {
        $result = $this->db->where('sub_id', $sub_id)->update('sub_groups', $data_insert);

        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }

    public function del_subGroup($sub_id = null)
    {
        $result = $this->db->where('sub_id', $sub_id)->delete('sub_groups');

        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }
}