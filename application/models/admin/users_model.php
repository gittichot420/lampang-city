<?php

defined('BASEPATH') or exit('No direct script access allowed');

class users_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_users($mem_status = null, $postData = null)
    {
        if ($mem_status == 0) {
            return $this->db->order_by('members.status ASC')->order_by('members.m_fname ASC')
                ->join('sub_groups', 'sub_groups.sub_id = members.sub_id', 'left')
                ->join('groups', 'groups.g_id = sub_groups.g_id', 'left')
                ->join('category', 'category.cat_id = members.m_type', 'left')
                ->select('members.m_id,members.m_title,members.m_fname,members.m_lname,members.m_card_id,
            members.m_phone,groups.g_id,groups.g_name,members.sub_id,sub_groups.sub_name,
            members.m_position,members.m_salary,members.m_type,category.cat_name,category.cat_type,members.status')
                ->get('members')->result_array();
        } else {
            return $this->db->order_by('members.status ASC')->order_by('members.m_fname ASC')
                ->where('members.status !=', '0')
                ->join('sub_groups', 'sub_groups.sub_id = members.sub_id', 'left')
                ->join('groups', 'groups.g_id = sub_groups.g_id', 'left')
                ->join('category', 'category.cat_id = members.m_type', 'left')
                ->select('members.m_id,members.m_title,members.m_fname,members.m_lname,members.m_card_id,
        members.m_phone,groups.g_id,groups.g_name,members.sub_id,sub_groups.sub_name,
        members.m_position,members.m_salary,members.m_type,category.cat_name,category.cat_type,members.status')
                ->get('members')->result_array();
        }
    }

    public function get_users_id($mem_id = null)
    {
        return $this->db->order_by('members.status ASC')->order_by('members.m_fname ASC')
            ->where('members.m_id', $mem_id)
            ->join('sub_groups', 'sub_groups.sub_id = members.sub_id', 'left')
            ->join('groups', 'groups.g_id = sub_groups.g_id', 'left')
            ->join('category', 'category.cat_id = members.m_type', 'left')
            ->select('members.m_id,members.m_title,members.m_fname,members.m_lname,members.m_card_id,
            members.m_phone,members.m_password,groups.g_id,groups.g_name,members.sub_id,sub_groups.sub_name,
            members.m_position,members.m_salary,members.m_type,category.cat_name,category.cat_type,members.status')
            ->get('members')->row_array();
    }
    public function check_card($card_id = null)
    {
        return $this->db->where('m_card_id', $card_id)->get('members')->row_array();
    }

    public function add_users($data_insert = null)
    {
        $result = $this->db->insert('members', $data_insert);

        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }

    public function resetSalary()
    {
        $data = ['m_salary' => 0];
        $this->db->update('members', $data);
    }
    public function delAllData()
    {
        $this->db->empty_table('tb_slip');
    }


    public function update_users($m_id = null, $data_insert = null)
    {
        $result = $this->db->where('m_id', $m_id)->update('members', $data_insert);

        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }

    public function del_users($m_id = null)
    {
        $result = $this->db->where('m_id', $m_id)->delete('members');

        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }
}
