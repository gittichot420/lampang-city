<?php

defined('BASEPATH') or exit('No direct script access allowed');

class employee_modal extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function employeeID($m_id = null)
    {
        return $this->db->order_by('members.m_fname ASC')
            ->where('members.m_id', $m_id)
            ->join('sub_groups', 'sub_groups.sub_id = members.sub_id', 'left')
            ->join('groups', 'groups.g_id = sub_groups.g_id', 'left')
            ->join('category', 'category.cat_id = members.m_type', 'left')
            ->get('members')->row_array();
    }
}