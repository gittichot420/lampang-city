<?php

defined('BASEPATH') or exit('No direct script access allowed');

class slip_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function slipMemArr($month = null, $m_id = [])
    {
        return $this->db->order_by('members.m_fname ASC')
            ->where('slip_date', $month)
            ->where_in('slip.m_id', $m_id)
            ->join('members', 'slip.m_id = members.m_id', 'left')
            ->join('sub_groups', 'sub_groups.sub_id = members.sub_id', 'left')
            ->join('groups', 'groups.g_id = sub_groups.g_id', 'left')
            ->join('category', 'members.m_type = category.cat_id', 'left')
            ->get('slip')->result_array();
    }

    public function allBefore($month = null, $m_id = [])
    {
        return $this->db->where('slip_date', $month)
            ->where_in('slip.m_id', $m_id)
            ->get('slip')->result_array();
    }

    public function userGroupType($group = null, $type = null)
    {
        return $this->db->order_by('members.m_fname ASC')
            ->where('groups.g_id', $group)
            ->where('cat_type', $type)
            ->join('sub_groups', 'sub_groups.sub_id = members.sub_id', 'left')
            ->join('groups', 'groups.g_id = sub_groups.g_id', 'left')
            ->join('category', 'category.cat_id = members.m_type', 'left')
            ->get('members')->result_array();
    }

    public function get_user_type($type = null)
    {
        return $this->db->order_by('members.m_fname ASC')
            ->where('cat_type', $type)
            ->join('sub_groups', 'sub_groups.sub_id = members.sub_id', 'left')
            ->join('groups', 'groups.g_id = sub_groups.g_id', 'left')
            ->join('category', 'category.cat_id = members.m_type', 'left')
            ->get('members')->result_array();
    }

    public function get_mem($m_id = null)
    {
        return $this->db->order_by('members.status ASC')->order_by('members.m_fname ASC')
            ->where('members.m_id', $m_id)
            ->join('sub_groups', 'sub_groups.sub_id = members.sub_id', 'left')
            ->join('groups', 'groups.g_id = sub_groups.g_id', 'left')
            ->join('category', 'category.cat_id = members.m_type', 'left')
            ->select('members.m_id,members.m_title,members.m_fname,members.m_lname,members.m_card_id,
        members.m_phone,members.m_password,groups.g_id,groups.g_name,members.sub_id,sub_groups.sub_name,
        members.m_position,members.m_salary,members.m_type,category.cat_name,category.cat_type,members.status')
            ->get('members')->row_array();
    }

    public function slip_m_id($month = null, $m_id = null)
    {
        return $this->db->order_by('members.m_fname ASC')
            ->where('slip_date', $month)
            ->where('slip.m_id', $m_id)
            ->join('members', 'slip.m_id = members.m_id', 'left')
            ->join('sub_groups', 'sub_groups.sub_id = members.sub_id', 'left')
            ->join('groups', 'groups.g_id = sub_groups.g_id', 'left')
            ->join('category', 'members.m_type = category.cat_id', 'left')
            ->get('slip')->row_array();
    }

    public function slip_mem($month = null, $m_id = null)
    {
        return $this->db->order_by('members.m_fname ASC')
            ->where('members.m_id', $m_id)
            ->join('category', 'members.m_type = category.cat_id', 'left')
            ->get('members')->row_array();
    }

    public function get_slip_type($id = null, $month = null)
    {
        return $this->db->where('m_id', $id)
            ->where('slip_date', $month)
            ->get('slip')->row_array();
    }

    public function addAll_slip($data_insert)
    {
        $result = $this->db->insert('slip', $data_insert);

        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }

    public function add_slip($data_insert = null)
    {
        $result = $this->db->insert('slip', $data_insert);

        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }
    public function update_slip($id = null, $data_insert = null)
    {
        $result = $this->db->where('slip_id', $id)->update('slip', $data_insert);

        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }

    public function del_assign($as_id = null)
    {
        $result = $this->db->where('as_id', $as_id)->delete('assign');

        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }
}
