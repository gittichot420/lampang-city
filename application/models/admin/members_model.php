<?php

defined('BASEPATH') or exit('No direct script access allowed');

class members_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_members()
    {
        return $this->db->order_by('u_fname', 'DESC')->get('users')->result_array();
    }

    public function get_users_id($u_id = null)
    {
        return $this->db->where('u_id', $u_id)->get('users')->row_array();
    }

    public function add_member($data_insert = null)
    {
        $result = $this->db->insert('users', $data_insert);

        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }

    public function update_users($u_id = null, $data_insert = null)
    {
        $result = $this->db->where('u_id', $u_id)->update('users', $data_insert);

        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }

    public function del_members($u_id = null)
    {
        $result = $this->db->where('u_id', $u_id)->delete('users');

        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }
}
