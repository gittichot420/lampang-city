<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>เริ่มต้นใช้งาน</title>
    <!-- css -->
    <link rel="stylesheet" href="<?= base_url('assets/css/app.css'); ?>">
    <!-- icon menubar -->
    <!-- <link rel="icon" type="image/png" href="<?= base_url('./assets/img/logo.gif') ?>" /> -->
    <link rel="stylesheet" href="<?= base_url('assets/plugins/fontawesome-free/css/all.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/adminlte.min.css'); ?>">
</head>

<style>
    .img-fluid {
        max-width: 20% !important;
    }
</style>

<body class="bg-form">
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="card bg-card mt-4" style="border-radius: 1rem;">
                    <div class="card-body text-black">
                        <div class="text-center p-4 mb-1 mt-n4">
                            <img src="<?= base_url('./assets/img/ICON-02.png'); ?>" alt="login form"
                                class="img-fluid" />
                        </div>
                        <div class="col-sm-12  text-center">
                            <span class="h2 fw-bold pb-3 font-weight-bold form-regis">สมัครสมาชิก</span>
                        </div>
                        <form action="<?= base_url('register') ?>" method="post">
                            <!-- row -->
                            <div class="row mt-5 px-3">
                                <div class="col-sm-12">
                                    <div class="form-outline mb-3">
                                        <label class="form-label form-regis" for="form2Example17">เลขบัตรประจำตัวประชาชน
                                            *</label>
                                        <div class="btn-group d-flex">
                                            <input id="card_id" name="card_id" type="number"
                                                onKeyPress="if(this.value.length==13) return false;" maxlength="13"
                                                class="form-control form-control" required style="width: 80%;" />
                                            <button type="button" class="btn btn-info btn-sm" id="btnVerify" onclick="verifyMember();$(this).html('processing...<i class=\'fa fa-cog faa-spin fa-spin fa-spinner\'></i>');">ยืนยัน</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-outline mb-3">
                                        <label class="form-label form-regis" for="form2Example17">คำนำหน้า</label>
                                        <input id="title" name="title" type="text"
                                            class="form-control form-control" readonly />
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-outline mb-3">
                                        <label class="form-label form-regis" for="form2Example17">ชื่อ</label>
                                        <input id="fname" name="fname" type="text"
                                            class="form-control form-control" readonly />
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-outline mb-3">
                                        <label class="form-label form-regis" for="form2Example17">นามสกุล</label>
                                        <input id="lname" name="lname" type="text"
                                            class="form-control form-control" readonly />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-outline mb-3">
                                        <label class="form-label form-regis" for="form2Example17">ตำแหน่ง</label>
                                        <input id="position" name="position" type="text"
                                            onKeyPress="if(this.value.length==13) return false;"
                                            class="form-control form-control" readonly />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label class="form-label form-regis" for="form2Example17">อีเมล* </label>
                                        <input id="email" name="email" type="email"
                                            class="form-control form-control" onkeyup='checkEmail();' required />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label class="form-label form-regis" for="form2Example17">ยืนยันอีเมล* </label>
                                        <input id="confirm_email" name="confirm_email" type="email"
                                            class="form-control form-control" onkeyup='checkEmail();' required />
                                        <small id='messageEmail' class="text-center"></small>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label class="form-label form-regis" for="form2Example17">รหัสผ่าน
                                            *</label>
                                        <input id="password" name="password" type="password"
                                            class="form-control form-control" onkeyup='checkPass();'
                                            pattern="^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$"
                                            title="ต้องมีตัวพิมพ์ใหญ่,ตัวพิมพ์เล็ก,ตัวเลขอย่างน้อยหนึ่งตัวและไม่น้อยกว่า 8 ตัวอักษร"
                                            required />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label class="form-label form-regis" for="form2Example17">ยืนยันรหัสผ่าน
                                            *
                                        </label>
                                        <input id="confirm_password" name="confirm_password" type="password"
                                            class="form-control form-control" onkeyup='checkPass();'
                                            pattern="^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$" />
                                        <small id='message' class="text-center"></small>
                                    </div>
                                </div>
                                <p><small id="emailHelp"
                                        class="form-text text-muted mb-3">*รหัสผ่านต้องเป็นภาษาอังกฤษตัวพิมพ์ใหญ่,
                                        ตัวพิมพ์เล็กและตัวเลขมากกว่า
                                        8 ตัวอักษร.</small></p>
                            </div>
                            <!-- end row -->
                            <div class="pt-1 mt-1 mb-4 px-3">
                                <button class="btn btn-primary btn-block" type="submit">สมัครสมาชิก</button>
                            </div>
                            <hr class="mt-4">
                            <div class="pt-1 text-center">
                                <p><a href="<?= base_url(); ?>" style="font-size: 18px;">ย้อนกลับ</a></p>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
</body>



<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
</script>
<!-- jQuery -->
<script src="<?= base_url('lib/jquery/dist/jquery.min.js'); ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?= base_url('assets/plugins/jquery-ui/jquery-ui.min.js'); ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<!-- <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> -->

<script>
    function verifyMember() {
        var card_id = $("#card_id").val();
        $.ajax({
            url: '/auth/checkID',
            type: 'POST',
            data: {
                card_id: card_id
            },
            dateType: 'json',
            success: function(respond, textStatus, jqXHR) {
                console.log(respond);
                if (respond.success == 0) {
                    // เจอ
                    // Swal.fire({
                    //     icon: 'success',
                    //     title: respond.msg,
                    //     showConfirmButton: true,
                    //     allowOutsideClick: false
                    // });
                    $("#card_id").val(respond.i);
                    $("#title").val(respond.prefix);
                    $("#fname").val(respond.fname);
                    $("#lname").val(respond.lname);
                    $("#position").val(respond.position);
                } else if (respond.success == 1) {
                    // ไม่เจอ
                    Swal.fire({
                        icon: 'error',
                        title: respond.msg,
                        showConfirmButton: true,
                        allowOutsideClick: false
                    });
                    $("#card_id").val('');
                    $("#title").val('');
                    $("#fname").val('');
                    $("#lname").val('');
                    $("#position").val('');
                } else if (respond.success == 2) {
                    // ไม่เจอ
                    Swal.fire({
                        icon: 'error',
                        title: respond.msg,
                        showConfirmButton: true,
                        allowOutsideClick: false
                    });
                    $("#card_id").val('');
                    $("#title").val('');
                    $("#fname").val('');
                    $("#lname").val('');
                    $("#position").val('');
                }
                // $("#responseMember").html(respond.msg);

                $("#btnVerify").html('ยืนยัน');
            }
        });
    }

    var checkPass = function() {
        var pass = $('#password').val();
        var confirm_password = $('#confirm_password').val();

        if (pass && confirm_password != null) {
            if (pass == confirm_password) {
                $('#message').css('color', 'green');
                $('#message').html(' รหัสผ่านตรง');
            } else {
                $('#message').css('color', 'red');
                $('#message').html(' รหัสผ่านไม่ตรงกัน');
            }
        } else {
            $('#message').html('');
        }
    }

    var checkEmail = function() {
        var email = $('#email').val();
        var confirm_email = $('#confirm_email').val();

        if (email && confirm_email != null) {
            if (email == confirm_email) {
                $('#messageEmail').css('color', 'green');
                $('#messageEmail').html(' อีเมลตรงกัน');
            } else {
                $('#messageEmail').css('color', 'red');
                $('#messageEmail').html(' อีเมลตรงกันไม่ตรงกัน');
            }
        } else {
            $('#messageEmail').html('');
        }
    }
</script>

<?php if ($this->session->flashdata('result') == 'success') {
    echo "<script>
        Swal.fire({
            icon: 'success',
            title: 'สมัครสมาชิกสำเร็จ',
            text: '" . $this->session->flashdata('message') . "', 
        })
    </script>";
} ?>
<?php if ($this->session->flashdata('result') == 'false') {
    echo "<script>
        Swal.fire({
            icon: 'error',
            title: 'สมัครสมาชิกไม่สำเร็จ',
            text: '" . $this->session->flashdata('message') . "',
        })
    </script>";
} ?>
<?php if ($this->session->flashdata('result') == 'duplicate') {
    echo "<script>
        Swal.fire({
            icon: 'warning',
            title: 'สมัครสมาชิกไม่สำเร็จ',
            text: '" . $this->session->flashdata('message') . "',
        })
    </script>";
} ?>

</html>