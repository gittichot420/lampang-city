<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>เริ่มต้นใช้งาน</title>
    <!-- css -->
    <link rel="stylesheet" href="<?= base_url('assets/css/app.css'); ?>">
    <!-- icon menubar -->
    <!-- <link rel="icon" type="image/png" href="<?= base_url('./assets/img/logo.gif') ?>" /> -->
    <link rel="stylesheet" href="<?= base_url('assets/plugins/fontawesome-free/css/all.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/adminlte.min.css'); ?>">
</head>

<body class="bg-form">
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6 mt-4">
                <div class="card bg-card mt-5" style="border-radius: 1rem;">
                    <div class="card-body text-black">
                        <div class="text-center p-4 mb-1 mt-n4">
                            <img src="<?= base_url('./assets/img/ICON-02.png'); ?>" alt="login form"
                                class="img-fluid" />
                        </div>
                        <div class="col-sm-12  text-center">
                            <span class="h2 fw-bold pb-3 font-weight-bold form-regis">สร้างรหัสผ่านใหม่</span>
                        </div>
                        <form action="<?= base_url('reset-pass') ?>" method="post">
                            <!-- row -->
                            <div class="row mt-5 px-3">
                                <div class="col-sm-12">
                                    <div class="form-outline">
                                        <label class="form-label form-regis" for="form2Example17">รหัสผ่าน
                                            *</label>
                                        <input id="password" name="password" type="password"
                                            class="form-control form-control" onkeyup='checkPass();'
                                            pattern="^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$"
                                            title="ต้องมีตัวพิมพ์ใหญ่,ตัวพิมพ์เล็ก,ตัวเลขอย่างน้อยหนึ่งตัวและไม่น้อยกว่า 8 ตัวอักษร"
                                            required />
                                    </div>
                                </div>
                                <div class="col-sm-12 mt-4">
                                    <div class="form-outline">
                                        <label class="form-label form-regis" for="form2Example17">ยืนยันรหัสผ่าน
                                            *
                                        </label>
                                        <input id="confirm_password" name="confirm_password" type="password"
                                            class="form-control form-control mb-1" onkeyup='checkPass();'
                                            pattern="^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$" />
                                        <span id='message' class="text-center"></span>
                                    </div>
                                </div>
                                <p><small id="emailHelp"
                                        class="form-text text-muted mb-3">*รหัสผ่านต้องเป็นภาษาอังกฤษตัวพิมพ์ใหญ่,
                                        ตัวพิมพ์เล็กและตัวเลขมากกว่า
                                        8 ตัวอักษร.</small></p>
                            </div>
                            <!-- end row -->
                            <div class="pt-1 mt-1 mb-4 px-3">
                                <button class="btn btn-primary btn-block" type="submit">ยืนยัน</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
</body>



<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
</script>
<!-- jQuery -->
<script src="<?= base_url('lib/jquery/dist/jquery.min.js'); ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?= base_url('assets/plugins/jquery-ui/jquery-ui.min.js'); ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script>
    var checkPass = function() {
        var pass = $('#password').val();
        var confirm_password = $('#confirm_password').val();

        if (pass && confirm_password != null) {
            if (pass == confirm_password) {
                $('#message').css('color', 'green');
                $('#message').html(' รหัสผ่านตรง');
            } else {
                $('#message').css('color', 'red');
                $('#message').html(' รหัสผ่านไม่ตรงกัน');
            }
        } else {
            $('#message').html('');
        }
    }
</script>

<?php if ($this->session->flashdata('result') == 'false') {
    echo "<script>
        Swal.fire({
            icon: 'error',
            title: 'ไม่สำเร็จ',
            text: '" . $this->session->flashdata('message') . "',
        })
    </script>";
} ?>

</html>