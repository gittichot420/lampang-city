<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper ">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row mb-5">
                <!-- Left col -->
                <div class="col-md-3 mt-4">
                    <!-- title -->
                    <form action="" method="get">
                        <div class="input-group">
                            <input class="form-control mr-2" style="border-radius: 5px " type="month" name="month"
                                value="<?= $month; ?>">
                            <button type="submit" class="btn btn-primary">ค้นหา</button>
                        </div>
                    </form>
                </div>
                <div class="col-md-9 mt-4">
                    <!-- title -->
                    <span class="text-secondary text-sm float-right"><a href="<?= base_url('admin/dashboard') ?>"
                            class="text-secondary txt-page">หน้าหลัก</a> >
                        <?php if ($type_name == 'ข้าราชการดำรงตำแหน่ง' || $type_name == 'พนักงานจ้าง') { ?>
                        <a href="<?= base_url('admin/dashboard/' . $group) ?>"
                            class="text-secondary txt-page">ประเภทพนักงาน</a> >
                        <?php } ?>
                        <?= $type_name ?>
                    </span>
                </div>
            </div>
            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <!-- <section class="col-lg-12 connectedSortable"> -->
                <section class="col-lg-12">
                    <!-- Custom tabs (Charts with tabs)-->
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <?php
                            $id = '';
                            $salary = '';
                            for ($i = 0; $i < count($user_type); $i++) {
                                if ($user_type[$i]['grand_total'] == null) {
                                    if ($i == count($user_type) - 1) {
                                        $id .= $user_type[$i]['m_id'];
                                        $salary .= $user_type[$i]['m_salary'];
                                    } else {
                                        $id .= $user_type[$i]['m_id'] . '/';
                                        $salary .= $user_type[$i]['m_salary'] . '/';
                                    }
                                }
                            }
                            ?>
                            <a <?php if( $id == null){
                                echo 'hidden';
                            } ?> class="btn btn-success" data-toggle="modal" data-target="#modal-specifically"
                                data-id="<?= $id; ?>" data-month="<?= $month; ?>" data-salary="<?= $salary; ?>"><i
                                    class=" fa fa-solid fa-check-double mr-1"></i>
                                ดำเนินการทั้งหมด</a>
                            <!-- group_assign -->
                            <div class="table-responsive">
                                <table id="table-slip" class="table" style="font-size: 14px !important;">
                                    <thead class="thead-color">
                                        <tr class="text-center table-tr">
                                            <th width="7%">
                                                ลำดับ
                                            </th>
                                            <th>ชื่อ-สกุล</th>
                                            <th>สำนัก/กอง</th>
                                            <th>สังกัด</th>
                                            <th>ตำแหน่ง</th>
                                            <th>รวมรับ</th>
                                            <th>รวมจ่าย</th>
                                            <th>คงเหลือ</th>
                                            <th>สถานะ</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $num = 0;
                                        foreach ($user_type as $value) {
                                            $num++ ?>
                                        <tr>
                                            <td class="text-center">
                                                <?= $num; ?>
                                            </td>
                                            <td>
                                                <?= $value['m_title'] . ' ' . $value['m_fname'] . ' ' . $value['m_lname']; ?>
                                            </td>
                                            <td>
                                                <?php if ($value['sub_id'] != null || $value['sub_id'] != 0) {
                                                        echo $value['g_name'];
                                                    } else {
                                                        echo '-';
                                                    } ?>
                                            </td>
                                            <td>
                                                <?php if ($value['sub_id'] != null || $value['sub_id'] != 0) {
                                                        echo $value['sub_name'];
                                                    } else {
                                                        echo '-';
                                                    } ?>
                                            </td>
                                            <td>
                                                <?php if ($value['m_position'] != null || $value['m_position'] != 0) {
                                                        echo $value['m_position'];
                                                    } else {
                                                        echo '-';
                                                    } ?>
                                            </td>
                                            <td class="text-right">
                                                <?= number_format($value['total'], 2) . ' ฿'; ?>
                                            </td>
                                            <td class="text-right">
                                                <?= number_format($value['grand_expense'], 2) . ' ฿'; ?>
                                            </td>
                                            <td class="text-right">
                                                <?= number_format($value['grand_total'], 2) . ' ฿'; ?>
                                            </td>
                                            <td class="text-center">
                                                <?php if ($value['grand_total'] != null) {
                                                        echo '<span class="badge badge-success badge-status">ดำเนินการสำเร็จ</span>';
                                                    } else {
                                                        echo ' <span class="badge badge-warning badge-status">รอดำเนินการ</span>';
                                                    } ?>
                                            </td>
                                            <td class="text-center">
                                                <a href="<?php if ($value['grand_total'] != null) {
                                                        echo base_url('admin/slip-update/') . $month . '/' . $value['m_id'];
                                                    } else {
                                                        echo base_url('admin/slip-add/') . $month . '/' . $value['m_id'];
                                                    } ?>" class="btn btn-warning">
                                                    <i class="fas fa-edit "></i></a>
                                                <?php if ($value['grand_total'] != null){ ?>
                                                <a href=" 
                                                        <?= base_url('admin/print-slip/') . $month . '/' . $value['m_id']; ?>
                                                   " class="btn btn-primary" target="_blank">
                                                    <i class="fas fa-print"></i>
                                                </a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </section>
                <!-- /.Left col -->
            </div>
            <!-- /.row (main row) -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>


<!-- Modal Del -->
<div class="modal fade" id="modal-specifically" data-backdrop="static" data-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="<?= base_url('admin/slip-all'); ?>" method="post">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <label style="font-size: 18px;padding-top: 40px;"><b>คุณต้องการดำเนินการออกบิลทั้งหมดใช่หรือไม่
                            !</b>
                    </label>
                    <input hidden type="text" id="m_id" name="m_id">
                    <input hidden type="text" id="month" name="month">
                    <input hidden type="text" id="salary" name="salary">
                </div>
                <div class="modal-footer" style="border-top: 0px;padding: 0px 10px 10px 0px;">
                    <button type="submit" class="btn btn-primary btn-sm">ยืนยัน</button>
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">ยกเลิก</button>
                </div>
            </form>
        </div>
    </div>
</div>