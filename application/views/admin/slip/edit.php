<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper ">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row mb-5">
                <!-- Left col -->
                <div class="col-md-4 mt-4">
                    <h5 class=" text-center" style="font-size:20px">
                        รายการเงินเดือนประจำเดือน
                        <span class="font-weight-bold pl-1" style="font-size:24px">
                            <?= $month_name; ?>
                        </span>
                    </h5>
                </div>
                <div class="col-md-8 mt-4">
                    <!-- title -->
                    <span class="text-secondary text-sm float-right"><a href="<?= base_url('admin/dashboard') ?>" class="text-secondary txt-page">หน้าหลัก</a> >
                        <a href="<?php if ($slip_admin['cat_type'] == 3) {
                                        echo base_url('admin/dashboard/999999');
                                    } else if ($slip_admin['cat_type'] == 1) {
                                        echo base_url('admin/dashboard/888888');
                                    } ?>" class="text-secondary txt-page">
                            <?= $slip_admin['cat_name']; ?>
                        </a> >
                        จัดการสลิป
                    </span>
                </div>
            </div>
            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <!-- <section class="col-lg-12 connectedSortable"> -->
                <section class="col-lg-12">
                    <!-- Custom tabs (Charts with tabs)-->
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <!-- About -->
                            <form action="<?= base_url('admin/slip/update/') . $slip_admin['slip_id']; ?>" method="get">
                                <div class="row g-0">
                                    <div class="col-md-12 col-lg-12 d-md-block">
                                        <div class="form-outline">
                                            <label for="" style="font-size:20px"> ชื่อ-สกุล :</label>
                                            <span style="font-size:20px">
                                                <?= $slip_admin['m_title'] . ' ' . $slip_admin['m_fname'] . ' ' . $slip_admin['m_lname']; ?>
                                            </span>
                                        </div>
                                        <div class="form-outline">
                                            <label for=""> ตำแหน่ง :</label>
                                            <span class="mr-2">
                                                <?= $slip_admin['m_position']; ?>
                                            </span>
                                            <label for=""> สำนัก/กอง :</label>
                                            <span class="mr-2">
                                                <?= $slip_admin['g_name']; ?>
                                            </span>
                                            <label for=""> สังกัด :</label>
                                            <span class="mr-2">
                                                <?= $slip_admin['sub_name']; ?>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6 d-flex align-items-center">
                                    </div>
                                </div>
                                <div class="row g-0 mt-4">
                                    <!-- Salary -->
                                    <div class="col-md-12 col-lg-5 d-md-block">
                                        <h5 class="text-center mb-4" for="" style="font-size:20px;font-weight: bold;color: green;">
                                            ประเภทรายจ่าย</h5>
                                        <div class="row g-0">
                                            <div class="col-7 col-sm-8 col-md-8 col-lg-8 d-md-block">
                                                <span for="" class="form-control form-control-sm form-span">
                                                    เงินเดือน : </span>
                                                <span for="" class="form-control form-control-sm form-span mt-3">
                                                    ปจต :
                                                </span>
                                                <span for="" class="form-control form-control-sm form-span mt-3">
                                                    เงินเพิ่มต่างๆ :
                                                </span>
                                                <span for="" class="form-control form-control-sm form-span mt-3">
                                                    ค่าตอบแทนพิเศษ :
                                                </span>
                                                <span for="" class="form-control form-control-sm form-span mt-3">
                                                    ค่าครองชีพชั่วคราว :
                                                </span>
                                                <span for="" class="form-control form-control-sm form-span mt-3">
                                                    เงินเพิ่มค่าปรับวุฒิ :
                                                </span>
                                                <span for="" class="form-control form-control-sm form-span mt-3">
                                                    อื่นๆ :
                                                </span>
                                            </div>
                                            <div class="col-5 col-sm-4 col-md-4 col-lg-4 d-md-block">
                                                <div class="income-total">
                                                    <input id="salary" name="salary" class="form-control form-control-sm form-group text-right" type="text" value="<?php if ($slip_admin['salary'] == 0) {
                                                                                                                                                                        echo $slip_admin['m_salary'];
                                                                                                                                                                    } else {
                                                                                                                                                                        echo $slip_admin['salary'];
                                                                                                                                                                    } ?>">
                                                    <?php $in_come = explode("/", $slip_admin['in_come']); ?>
                                                    <input id="in_come1" name="in_come1" class="form-control form-control-sm form-group text-right" type="text" value="<?= $in_come[0] ?>" step="any">
                                                    <input id="in_come2" name="in_come2" class="form-control form-control-sm form-group text-right" type="text" value="<?= $in_come[1] ?>" step="any">
                                                    <input id="in_come3" name="in_come3" class="form-control form-control-sm form-group text-right" type="text" value="<?= $in_come[2] ?>" step="any">
                                                    <input id="in_come4" name="in_come4" class="form-control form-control-sm form-group text-right" type="text" value="<?= $in_come[3] ?>" step="any">
                                                    <input id="in_come5" name="in_come5" class="form-control form-control-sm form-group text-right" type="text" value="<?= $in_come[4] ?>" step="any">
                                                    <input id="in_come6" name="in_come6" class="form-control form-control-sm form-group text-right" type="text" value="<?= isset($in_come[5]) ? $in_come[5] : '0' ?>" step="any">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Expense -->
                                    <div class="col-md-12 col-lg-7 d-md-block">
                                        <h5 class="text-center mb-4" for="" style="font-size:20px;font-weight: bold;color: red;">
                                            ประเภทเงินที่หัก</h5>
                                        <div class="row g-0">
                                            <div class="col-7 col-sm-8 col-md-8 col-lg-8 d-md-block">
                                                <span for="1" class="form-control form-control-sm form-span">
                                                    ภาษี ณ ที่จ่าย :
                                                </span>
                                                <span for="2" class="form-control form-control-sm form-span mt-3">
                                                    ประกันสังคม :
                                                </span>
                                                <span for="3" class="form-control form-control-sm form-span mt-3">
                                                    กบข. :
                                                </span>
                                                <span for="4" class="form-control form-control-sm form-span mt-3">
                                                    กสจ. :
                                                </span>
                                                <span for="5" class="form-control form-control-sm form-span mt-3">
                                                    การฌาปนกิจสงเคราะห์ ปภ. :
                                                </span>
                                                <span for="6" class="form-control form-control-sm form-span mt-3">
                                                    ธนาคารออมสิน สาขาบิ๊กซีลำปาง :
                                                </span>
                                                <span for="7" class="form-control form-control-sm form-span mt-2 mb-4">
                                                    การฌาปนกิจสงเคราะห์ข้าราชการและบุคลากรท้องถิ่น (ก.ฌ.) :
                                                </span>
                                                <span for="8" class="form-control form-control-sm form-span mt-4">
                                                    ธนาคารอาคารสงเคราะห์ สาขาลำปาง :
                                                </span>
                                                <span for="9" class="form-control form-control-sm form-span mt-3">
                                                    ธนาคารออมสิน สาขาลำปาง :
                                                </span>
                                                <span for="10" class="form-control form-control-sm form-span mt-3">
                                                    ธนาคารออมสิน สาขาสบตุ๋ย :
                                                </span>
                                                <span for="11" class="form-control form-control-sm form-span mt-3">
                                                    ธนาคารกรุงไทย จำกัด (มหาชน) สาขาลำปาง :
                                                </span>
                                                <span for="12" class="form-control form-control-sm form-span mt-3 ">
                                                    ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร สาขาลำปาง :
                                                </span>
                                                <span for="13" class="form-control form-control-sm form-span mt-3">
                                                    กองทุนบำเหน็จบำนาญข้าราชการ :
                                                </span>
                                                <span for="14" class="form-control form-control-sm form-span mt-3">
                                                    สหกรณ์ออมทรัพย์พนักงานเทศบาล จำกัด :
                                                </span>
                                                <span for="15" class="form-control form-control-sm form-span mt-3">
                                                    สหกรณ์ออมทรัพย์กรมโยธาธิการ 2529 จำกัด :
                                                </span>
                                                <span for="16" class="form-control form-control-sm form-span mt-3">
                                                    สหกรณ์ออมทรัพย์ รพช. จำกัด :
                                                </span>
                                                <span for="17" class="form-control form-control-sm form-span mt-3">
                                                    กองทุนสวัสดิการ พนักงานและลูกจ้าง (กองคลัง) :
                                                </span>
                                                <span for="18" class="form-control form-control-sm form-span mt-3">
                                                    ช.พ.ค.ลำปาง :
                                                </span>
                                                <span for="19" class="form-control form-control-sm form-span mt-3">
                                                    ช.พ.ส. :
                                                </span>
                                                <span for="20" class="form-control form-control-sm form-span mt-3">
                                                    สมาคม ชค.ลป. :
                                                </span>
                                                <span for="21" class="form-control form-control-sm form-span mt-3">
                                                    สหกรณ์ออมทรัพย์พนักงานครูลำปาง :
                                                </span>
                                                <span for="22" class="form-control form-control-sm form-span mt-3">
                                                    เทศบาลตำบลวังเหนือ :
                                                </span>
                                                <span for="23" class="form-control form-control-sm form-span mt-3">
                                                    เงินสวัสดิการสำนักการศึกษา :
                                                </span>
                                                <span for="24" class="form-control form-control-sm form-span mt-3">
                                                    เงินกองทุนสวัสดิการกองการศึกษา :
                                                </span>
                                                <span for="25" class="form-control form-control-sm form-span mt-3">
                                                    สวัสดิการสำนักการศึกษา :
                                                </span>
                                                <span for="26" class="form-control form-control-sm form-span mt-3">
                                                    ธนาคารออมสิน สาขาเซ็นทรัลลำปาง :
                                                </span>
                                                <span for="27" class="form-control form-control-sm form-span mt-3">
                                                    ธนาคารอิสลาม :
                                                </span>
                                                <span for="28" class="form-control form-control-sm form-span mt-3">
                                                    กลุ่มพัฒนาอาชีพ :
                                                </span>
                                                <span for="29" class="form-control form-control-sm form-span mt-3">
                                                    เงินสะสม :
                                                </span>
                                                <span for="30" class="form-control form-control-sm form-span mt-3">
                                                    สหกรณ์ออมทรัพย์องค์กรปกครองส่วนท้องถิ่น จำกัด :
                                                </span>
                                                <span for="31" class="form-control form-control-sm form-span mt-3">
                                                    สหกรณ์ออมทรัพย์พนักงานองค์การปกครองส่วนท้องถิ่น จังหวัดลำปาง :
                                                </span>
                                                <span for="32" class="form-control form-control-sm form-span mt-3">
                                                    กองทุนเงินให้กู้ยืมเพื่อการศึกษา :
                                                </span>
                                                <span for="33" class="form-control form-control-sm form-span mt-3">
                                                    สวัสดิการครูโรงเรียนเทศบาล 1 :
                                                </span>
                                                <span for="34" class="form-control form-control-sm form-span mt-3">
                                                    สวัสดิการครูโรงเรียนเทศบาล 4 :
                                                </span>
                                                <span for="35" class="form-control form-control-sm form-span mt-3">
                                                    เทศบาลนครลำปาง :
                                                </span>
                                                <span for="36" class="form-control form-control-sm form-span mt-3">
                                                    สำนักงานส่งเสริมการปกครองท้องถิ่นจังหวัดลำปาง :
                                                </span>
                                                <span for="37" class="form-control form-control-sm form-span mt-3">
                                                    รร.เทศบาล 6 (กองทุนสวัสดิการ) :
                                                </span>
                                                <span for="38" class="form-control form-control-sm form-span mt-3">
                                                    สหกรณ์ออมทรัพย์ข้าราชการองค์การบริหารส่วนจังหวัด จำกัด :
                                                </span>
                                                <span for="39" class="form-control form-control-sm form-span mt-3">
                                                    สหกรณ์ออมทรัพย์ครูกรมสามัญศึกษา จ.เลย :
                                                </span>
                                                <span for="40" class="form-control form-control-sm form-span mt-3">
                                                    สหกรณ์ออมทรัพย์ครูระยอง :
                                                </span>
                                                <span for="41" class="form-control form-control-sm form-span mt-3">
                                                    สหกรณ์ออมทรัพย์ข้าราชการกระทรวงศึกษาธิการเชียงใหม่ จำกัด :
                                                </span>
                                                <span for="42" class="form-control form-control-sm form-span mt-3">
                                                    ฌาปนกิจสงเคราะห์ข้าราชการส่วนท้องถิ่นเชียงใหม่ :
                                                </span>
                                                <span for="43" class="form-control form-control-sm form-span mt-3">
                                                    สหกรณ์ออมทรัพย์กระทรวงแรงงาน จำกัด :
                                                </span>
                                                <span for="44" class="form-control form-control-sm form-span mt-3">
                                                    ชมรม บน. :
                                                </span>
                                                <span for="45" class="form-control form-control-sm form-span mt-3">
                                                    เบ็ดเตล็ด :
                                                </span>
                                                <span for="46" class="form-control form-control-sm form-span mt-3">
                                                    อื่นๆ :
                                                </span>
                                            </div>
                                            <div class="col-5 col-sm-4 col-md-4 col-lg-4 d-md-block">
                                                <div class="expense-total">
                                                    <?php $expense = explode("/", $slip_admin['expense']); ?>
                                                    <input id="expense1" name="expense1" class="form-control form-control-sm form-group text-right" type="text" value="<?= $expense[0] ?>" step="any">
                                                    <input id="expense2" name="expense2" class="form-control form-control-sm form-group text-right" type="text" value="<?= $expense[1] ?>" step="any">
                                                    <input id="expense3" name="expense3" class="form-control form-control-sm form-group text-right" type="text" value="<?= $expense[2] ?>" step="any">
                                                    <input id="expense4" name="expense4" class="form-control form-control-sm form-group text-right" type="text" value="<?= $expense[3] ?>" step="any">
                                                    <input id="expense5" name="expense5" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[4] ?>" step="any">
                                                    <input id="expense6" name="expense6" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[5] ?>" step="any">
                                                    <input id="expense7" name="expense7" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[6] ?>" step="any">
                                                    <input id="expense8" name="expense8" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[7] ?>" step="any">
                                                    <input id="expense9" name="expense9" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[8] ?>" step="any">
                                                    <input id="expense10" name="expense10" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[9] ?>" step="any">
                                                    <input id="expense11" name="expense11" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[10] ?>" step="any">
                                                    <input id="expense12" name="expense12" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[11] ?>" step="any">
                                                    <input id="expense13" name="expense13" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[12] ?>" step="any">
                                                    <input id="expense14" name="expense14" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[13] ?>" step="any">
                                                    <input id="expense15" name="expense15" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[14] ?>" step="any">
                                                    <input id="expense16" name="expense16" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[15] ?>" step="any">
                                                    <input id="expense17" name="expense17" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[16] ?>" step="any">
                                                    <input id="expense18" name="expense18" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[17] ?>" step="any">
                                                    <input id="expense19" name="expense19" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[18] ?>" step="any">
                                                    <input id="expense20" name="expense20" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[19] ?>" step="any">
                                                    <input id="expense21" name="expense21" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[20] ?>" step="any">
                                                    <input id="expense22" name="expense22" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[21] ?>" step="any">
                                                    <input id="expense23" name="expense23" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[22] ?>" step="any">
                                                    <input id="expense24" name="expense24" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[23] ?>" step="any">
                                                    <input id="expense25" name="expense25" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[24] ?>" step="any">
                                                    <input id="expense26" name="expense26" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[25] ?>" step="any">
                                                    <input id="expense27" name="expense27" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[26] ?>" step="any">
                                                    <input id="expense28" name="expense28" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[27] ?>" step="any">
                                                    <input id="expense29" name="expense29" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[28] ?>" step="any">
                                                    <input id="expense30" name="expense30" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[29] ?>" step="any">
                                                    <input id="expense31" name="expense31" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[30] ?>" step="any">
                                                    <input id="expense32" name="expense32" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[31] ?>" step="any">
                                                    <input id="expense33" name="expense33" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[32] ?>" step="any">
                                                    <input id="expense34" name="expense34" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[33] ?>" step="any">
                                                    <input id="expense35" name="expense35" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[34] ?>" step="any">
                                                    <input id="expense36" name="expense36" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[35] ?>" step="any">
                                                    <input id="expense37" name="expense37" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[36] ?>" step="any">
                                                    <input id="expense38" name="expense38" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[37] ?>" step="any">
                                                    <input id="expense39" name="expense39" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[38] ?>" step="any">
                                                    <input id="expense40" name="expense40" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[39] ?>" step="any">
                                                    <input id="expense41" name="expense41" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[40] ?>" step="any">
                                                    <input id="expense42" name="expense42" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[41] ?>" step="any">
                                                    <input id="expense43" name="expense43" class="form-control form-control-sm form-group  text-right" type="text" value="<?= $expense[42] ?>" step="any">
                                                    <input id="expense44" name="expense44" class="form-control form-control-sm form-group  text-right" type="text" value="<?= isset($expense[43]) ? $expense[43] : '0'; ?>" step="any">
                                                    <input id="expense45" name="expense45" class="form-control form-control-sm form-group  text-right" type="text" value="<?= isset($expense[44]) ? $expense[44] : '0'; ?>" step="any">
                                                    <input id="expense46" name="expense46" class="form-control form-control-sm form-group  text-right" type="text" value="<?= isset($expense[45]) ? $expense[45] : '0'; ?>" step="any">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row g-0 mt-4">
                                    <!-- Salary -->
                                    <div class="col-md-12 col-lg-6 d-md-block">
                                        <div class="row g-0">
                                            <div class="col-sm-0 col-md-0 col-lg-5 d-md-block">
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-3 d-md-block">
                                                <span for="" class="form-control form-span text-bold ">
                                                    รวมรับ : </span>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-4 d-md-block">
                                                <input id="total" name="total" class="form-control form-group  text-right" type="text" value="<?php if ($slip_admin['total'] == 0) {
                                                                                                                                                    echo number_format($slip_admin['m_salary'], 2);
                                                                                                                                                } else {
                                                                                                                                                    echo number_format($slip_admin['total'], 2);
                                                                                                                                                } ?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Expense -->
                                    <div class="col-md-12 col-lg-6 d-md-block">
                                        <div class="row g-0">
                                            <div class="col-sm-0 col-md-0 col-lg-1 d-md-block">
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-7 d-md-block">
                                                <span for="1" class="form-control form-span text-bold ">
                                                    รวมจ่าย :
                                                </span>
                                                <span for="2" class="form-control form-span text-bold mt-3">
                                                    คงเหลือ :
                                                </span>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-4 d-md-block">
                                                <input id="total_expense" name="total_expense" class="form-control form-group text-right" type="text" value="<?= number_format($slip_admin['grand_expense'], 2); ?>" readonly>
                                                <input id="grand_total" name="grand_total" class="form-control form-group text-right" type="text" value="<?= number_format($slip_admin['grand_total'], 2); ?>" onkeyup="" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row g-0">
                                    <div class="col-md-12 col-lg-12 d-md-block text-center">
                                        <div class="form-outline">
                                            <input hidden type="text" name="s_month" value="<?= $month ?>">
                                            <input hidden type="text" name="s_date" value="<?= $slip_admin['slip_date'] ?>">
                                            <input hidden type="text" name="m_id" value="<?= $slip_admin['m_id'] ?>">
                                            <button type="submit" class="btn btn-success">บันทึก</button>
                                            <input action="action" class="btn btn-primary" onclick="window.history.go(-1); return false;" type="submit" value="ย้อนกลับ" />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </section>
            </div>
            <!-- /.card -->
            <!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

<script>
    const numberFormatter = new Intl.NumberFormat('en-US', {
        style: 'decimal',
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
    });

    $('.income-total input, .expense-total input').keyup(function() {
        // รายรับ
        var salary = parseFloat($('#salary').val()) || 0;
        var in_come1 = parseFloat($('#in_come1').val()) || 0;
        var in_come2 = parseFloat($('#in_come2').val()) || 0;
        var in_come3 = parseFloat($('#in_come3').val()) || 0;
        var in_come4 = parseFloat($('#in_come4').val()) || 0;
        var in_come5 = parseFloat($('#in_come5').val()) || 0;
        var in_come6 = parseFloat($('#in_come6').val()) || 0;
        var sum_total = salary + in_come1 + in_come2 + in_come3 + in_come4 + in_come5 + in_come6;
        const formattedNumber = numberFormatter.format(sum_total);
        $('#total').val(formattedNumber);

        // รายจ่าย
        var ex1 = parseFloat($('#expense1').val()) || 0;
        var ex2 = parseFloat($('#expense2').val()) || 0;
        var ex3 = parseFloat($('#expense3').val()) || 0;
        var ex4 = parseFloat($('#expense4').val()) || 0;
        var ex5 = parseFloat($('#expense5').val()) || 0;
        var ex6 = parseFloat($('#expense6').val()) || 0;
        var ex7 = parseFloat($('#expense7').val()) || 0;
        var ex8 = parseFloat($('#expense8').val()) || 0;
        var ex9 = parseFloat($('#expense9').val()) || 0;
        var ex10 = parseFloat($('#expense10').val()) || 0;
        var ex11 = parseFloat($('#expense11').val()) || 0;
        var ex12 = parseFloat($('#expense12').val()) || 0;
        var ex13 = parseFloat($('#expense13').val()) || 0;
        var ex14 = parseFloat($('#expense14').val()) || 0;
        var ex15 = parseFloat($('#expense15').val()) || 0;
        var ex16 = parseFloat($('#expense16').val()) || 0;
        var ex17 = parseFloat($('#expense17').val()) || 0;
        var ex18 = parseFloat($('#expense18').val()) || 0;
        var ex19 = parseFloat($('#expense19').val()) || 0;
        var ex20 = parseFloat($('#expense20').val()) || 0;
        var ex21 = parseFloat($('#expense21').val()) || 0;
        var ex22 = parseFloat($('#expense22').val()) || 0;
        var ex23 = parseFloat($('#expense23').val()) || 0;
        var ex24 = parseFloat($('#expense24').val()) || 0;
        var ex25 = parseFloat($('#expense25').val()) || 0;
        var ex26 = parseFloat($('#expense26').val()) || 0;
        var ex27 = parseFloat($('#expense27').val()) || 0;
        var ex28 = parseFloat($('#expense28').val()) || 0;
        var ex29 = parseFloat($('#expense29').val()) || 0;
        var ex30 = parseFloat($('#expense30').val()) || 0;
        var ex31 = parseFloat($('#expense31').val()) || 0;
        var ex32 = parseFloat($('#expense32').val()) || 0;
        var ex33 = parseFloat($('#expense33').val()) || 0;
        var ex34 = parseFloat($('#expense34').val()) || 0;
        var ex35 = parseFloat($('#expense35').val()) || 0;
        var ex36 = parseFloat($('#expense36').val()) || 0;
        var ex37 = parseFloat($('#expense37').val()) || 0;
        var ex38 = parseFloat($('#expense38').val()) || 0;
        var ex39 = parseFloat($('#expense39').val()) || 0;
        var ex40 = parseFloat($('#expense40').val()) || 0;
        var ex41 = parseFloat($('#expense41').val()) || 0;
        var ex42 = parseFloat($('#expense42').val()) || 0;
        var ex43 = parseFloat($('#expense43').val()) || 0;
        var ex44 = parseFloat($('#expense44').val()) || 0;
        var ex45 = parseFloat($('#expense45').val()) || 0;
        var ex46 = parseFloat($('#expense46').val()) || 0;
        var sum_ex = ex1 + ex2 + ex3 + ex4 + ex5 + ex6 + ex7 + ex8 + ex9 + ex10 + ex11 +
            ex12 + ex13 +
            ex14 + ex15 + ex16 + ex17 + ex18 + ex19 + ex20 + ex21 + ex22 + ex23 + ex24 +
            ex25 + ex26 +
            ex27 + ex28 + ex29 + ex30 + ex31 + ex32 + ex33 + ex34 + ex35 + ex36 + ex37 +
            ex38 + ex39 +
            ex40 + ex41 + ex42 + ex43 + ex44 + ex45 + ex46;
        const formatNum = numberFormatter.format(sum_ex);
        $('#total_expense').val(formatNum);


        // คงเหลือ
        var grand_total = sum_total - sum_ex;
        const formatNew = numberFormatter.format(grand_total);
        $('#grand_total').val(formatNew);
    });
</script>