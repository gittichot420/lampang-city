<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper ">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row mb-5">
                <!-- Left col -->
                <div class="col-md-3 mt-4">
                </div>
                <div class="col-md-9 mt-4">
                    <!-- title -->
                    <span class="text-secondary text-sm float-right"><a href="<?= base_url('admin/dashboard') ?>"
                            class="text-secondary txt-page">หน้าหลัก</a> > ประเภทพนักงาน</span>
                </div>
            </div>
            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <!-- <section class="col-lg-12 connectedSortable"> -->
                <section class="col-lg-12">
                    <!-- Custom tabs (Charts with tabs)-->
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <!-- group_assign -->
                            <div class="table-responsive">
                                <table id="get_users" class="table">
                                    <thead class="thead-color">
                                        <tr class="text-center table-tr">
                                            <th colspan="2">ประเภทพนักงาน</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="text-center">
                                            <td class="fulltd" style="width:50%">
                                                <div class="button_cont mt-1" align="center">
                                                    <a href="<?= base_url('admin/slip-admin/') . $g_id . '/0'; ?>"
                                                        class="example_e">
                                                        <div class="card bg-primary mx-1 py-5">
                                                            <div class="card-body ">
                                                                <i class="far fa-address-card"
                                                                    style="font-size:80px"></i>
                                                                <h2 class="pt-2 font-weight-bolder fulltd">
                                                                    ข้าราชการ
                                                                </h2>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </td>
                                            <td class="fulltd" style="width:50%">
                                                <div class="button_cont mt-1" align="center">
                                                    <a href="<?= base_url('admin/slip-admin/') . $g_id . '/2'; ?>"
                                                        class="example_e">
                                                        <div class="card bg-orange mx-1 py-5">
                                                            <div class="card-body ">
                                                                <i class="far fa-address-card"
                                                                    style="font-size:80px"></i>
                                                                <h2 class="pt-2 font-weight-bolder fulltd">
                                                                    พนักงานจ้าง
                                                                </h2>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </section>
                <!-- /.Left col -->
            </div>
            <!-- /.row (main row) -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>