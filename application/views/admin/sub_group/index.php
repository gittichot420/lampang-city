<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- Left col -->
                <div class="col-md-12 mt-4">
                    <!-- title -->
                    <h2 class="float-left txt-tilte-page"><i class="far fa-list-alt nav-icon"></i> สำนัก/กอง
                        (
                        <?= $groupName['g_name']; ?>)
                    </h2>
                    <span class="text-secondary text-sm float-right"><a href="<?= base_url('admin/dashboard') ?>"
                            class="text-secondary txt-page">หน้าหลัก</a> > <a href="<?= base_url('admin/group') ?>"
                            class="text-secondary txt-page">สำนัก/กอง</a> > สังกัด</span>
                </div>
            </div>
            <!-- CONTENT -->
            <div class="card mt-4">
                <div class="card-header">
                    <div class="card-tools">
                        <ul class="nav nav-pills ml-auto">
                            <li class="nav-item">
                                <a href="" class="btn btn-success btn-sm" title="ลบ" data-id="" data-toggle="modal"
                                    data-target="#modal-create"><i class="fas fa-plus mr-1"></i> เพิ่มข้อมูล</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="data-table" class="table">
                            <thead class="thead-color">
                                <tr class="text-center">
                                    <th width="15%" style="text-align:center !important;padding-right: 10px;">ลำดับ
                                    </th>
                                    <th>ชื่อสังกัด</th>
                                    <th width="30%"></th>
                                </tr>
                            </thead>
                            <tbody class="table-bordered">
                                <?php
                                $num = 0;
                                if (isset($subGroupId)) {
                                    foreach ($subGroupId as $row) {
                                        $num++;
                                        ?>
                                        <tr>
                                            <td class="text-center font-weight-bold">
                                                <?= $num; ?>
                                            </td>
                                            <td style="font-size:16px">
                                                <?= $row['sub_name']; ?>
                                            </td>
                                            <td class="text-center font-weight-bold">
                                                <div class="btn-group">
                                                    <a href="" class="btn btn-warning btn-sm" title="แก้ไข" data-toggle="modal"
                                                        data-target="#edit-list" data-id="<?= $row['sub_id']; ?>"
                                                        data-name="<?= $row['sub_name']; ?>" data-group="<?= $row['g_id'] ?>"
                                                        data-url="<?= base_url('admin/sub-group/update/') . $row['g_id']; ?>"><i
                                                            class="fas fa-edit mr-1"></i>
                                                        แก้ไข</a>
                                                    <a href="" class="btn btn-danger btn-sm" title="ลบ" data-toggle="modal"
                                                        data-target="#del-list" data-id="<?= $row['sub_id']; ?>"
                                                        data-url="<?= base_url('admin/sub-group/del/') . $row['g_id']; ?>"><i
                                                            class="fas fa-trash mr-1"></i> ลบ</a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php }
                                } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    </section>
</div>
</div>
</section>
</div>