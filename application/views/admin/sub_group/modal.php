<!-- Modal Add-->
<div class="modal fade" id="modal-create" data-backdrop="static" data-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body p-4">
                <form action="<?= base_url('admin/sub-group/add'); ?>" method="post">
                    <div class="form-outline float-right mt-n4">
                        <button type="button" class="close mt-n2" data-dismiss="modal" aria-label="Close">
                            <span class="txt-mark" aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <input hidden type="text" id="g_id" name="g_id" value="<?= $groupName['g_id']; ?>">
                    <div class="form-outline mt-3 mb-4">
                        <label class="label-group" for="form2Example17">ชื่อสังกัด</label>
                        <input id="sub_group" name="sub_group" type="text" class="form-control" placeholder=""
                            required />
                    </div>
                    <div class="float-right mt-3">
                        <button type="submit" class="btn btn-success">บันทึก</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Edit-->
<div class="modal fade" id="edit-list" data-backdrop="static" data-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body p-4">
                <form id="edit_url" action="" method="get">
                    <div class="form-outline float-right mt-n4">
                        <button type="button" class="close mt-n2" data-dismiss="modal" aria-label="Close">
                            <span class="txt-mark" aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <input hidden type="text" id="edit_id" name="sub_id">
                    <div class="form-outline mt-3">
                        <label class="label-group" for="form2Example17">ชื่อสำนัก/กอง</label>
                        <select id="group_list" name="g_id" class="custom-select">
                            <?php foreach ($get_group as $row) { ?>
                                <option id="<?= $row['g_id'] ?>" value="<?= $row['g_id'] ?>">
                                    <?= $row['g_name'] ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-outline mt-3 mb-4">
                        <label class="label-group" for="form2Example17">ชื่อสังกัด</label>
                        <input id="edit_name" name="sub_name" type="text" class="form-control" placeholder=""
                            required />
                    </div>
                    <div class="float-right mt-3">
                        <button type="submit" class="btn btn-success">บันทึก</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Del -->
<div class="modal fade" id="del-list" data-backdrop="static" data-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form id="del_url" action="" method="get">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <label style="padding-top: 40px;"><b>คุณต้องการลบข้อมูลรายการนี้ใช่หรือไม่?</b>
                    </label>
                    <input hidden type="text" id="del_id" name="sub_id">
                </div>
                <div class="modal-footer" style="border-top: 0px;padding: 0px 10px 10px 0px;">
                    <button type="submit" class="btn btn-primary btn-sm">ยืนยัน</button>
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">ยกเลิก</button>
                </div>
            </form>
        </div>
    </div>
</div>