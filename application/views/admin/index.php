<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper ">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row mb-5">
                <!-- Left col -->
                <div class="col-md-3 mt-4">
                    <!-- title -->
                    <form action="" method="get">
                        <div class="input-group">
                            <input class="form-control mr-2" style="border-radius: 5px " type="month" name="month" value="<?= $month; ?>">
                            <button type="submit" class="btn btn-primary">ค้นหา</button>
                        </div>
                    </form>
                </div>
                <div class="col-md-9 mt-4">
                    <span class="text-secondary text-sm float-right">หน้าหลัก</span>
                </div>
            </div>

            <!-- Small boxes (Stat box) -->
            <div class="row">
                <!-- Earnings (Monthly) Card Example -->
                <div class="col-xl-4 col-md-6 mb-4">
                    <div class="info-box py-3 px-3">
                        <span class="info-box-icon bg-primary elevation-1 px-5">
                            <i class="fas fa-clipboard-list fa-2x"></i>
                            <!-- <i class="fas fa-user-edit"></i> -->
                        </span>
                        <div class="info-box-content pl-3">
                            <span class="info-box-text text-primary text-bold" style="font-size:25px">ทั้งหมด</span>
                            <span class="info-box-number p-0 m-0" style="font-size:20px">
                                <?php if ($sumALl != null) {
                                    echo $sumALl;
                                } else {
                                    echo '0';
                                } ?>
                                <small>คน</small>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-6 mb-4">
                    <div class="info-box py-3 px-3">
                        <span class="info-box-icon bg-success elevation-1 px-5">
                            <i class="fas fa-clipboard-check fa-2x"></i>
                        </span>
                        <div class="info-box-content pl-3">
                            <span class="info-box-text text-bold text-success" style="font-size:25px">ดำเนินการเสร็จสิ้น</span>
                            <span class="info-box-number p-0 m-0" style="font-size:20px">
                                <?php if ($sumSuccess != null) {
                                    echo $sumSuccess;
                                } else {
                                    echo '0';
                                } ?>
                                <small>คน</small>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-6 mb-4">
                    <div class="info-box py-3 px-3">
                        <span class="info-box-icon bg-warning elevation-1 px-5">
                            <i class="fas fa-retweet fa-2x text-white"></i>
                        </span>
                        <div class="info-box-content pl-3">
                            <span class="info-box-text text-bold text-warning" style="font-size:25px">รอดำเนินการ</span>
                            <span class="info-box-number p-0 m-0" style="font-size:20px">
                                <?php if ($sumSuccess != null) {
                                    echo $sumALl - $sumSuccess;
                                } else {
                                    echo $sumALl;
                                } ?>
                                <small>คน</small>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <!-- Main row -->

            <div class="row">
                <!-- Left col -->
                <!-- <section class="col-lg-12 connectedSortable"> -->
                <section class="col-lg-12">
                    <!-- Custom tabs (Charts with tabs)-->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <i class="far fa-calendar-alt mr-1"></i>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <!-- group_assign -->
                            <div class="table-responsive">
                                <table id="get_users" class="table table-bordered">
                                    <thead class="thead-color">
                                        <tr class="text-center table-tr">
                                            <th colspan="<?= count($group_assign); ?>">สำนัก/กอง ที่รับผิดชอบ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="text-center">
                                            <?php if (isset($group_assign)) {
                                                if (count($group_assign) != 0) {
                                                    $width = 100 / count($group_assign) . '%';
                                                }
                                                foreach ($group_assign as $row) { ?>
                                                    <td class="fulltd" style="font-size:16px;width:<?= $width; ?>">
                                                        <a href="<?= base_url('admin/dashboard/') . $row['g_id']; ?>" class="g-name pr-4" title="ดูรายการสินค้า">
                                                            <div class="fulltd">
                                                                <?= $row['g_name']; ?>
                                                            </div>
                                                        </a>
                                                    </td>
                                            <?php }
                                            } ?>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </section>
                <!-- /.Left col -->
            </div>
            <!-- /.row (main row) -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>