<!-- Content Wrapper. Contains page content -->
<style>
/* #get_users td:nth-child(7),
th:nth-child(7) {
    display: none;
} */
</style>

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- Left col -->
                <div class="col-md-12 mt-4">
                    <!-- title -->
                    <h2 class="float-left txt-tilte-page"><i class="fas fa-users-cog nav-icon"></i> กำหนดสิทธิ์
                    </h2>
                    <span class="text-secondary text-sm float-right"><a href="<?= base_url('admin/dashboard') ?>"
                            class="text-secondary txt-page">หน้าหลัก</a> > กำหนดสิทธิ์</span>
                </div>
            </div>
            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <section class="col-md-12">
                    <!-- DIRECT CHAT -->
                    <div class="card mt-4">
                        <div class="card-header">
                            <div class="card-tools">
                                <ul class="nav nav-pills ml-auto">
                                    <li class="nav-item">
                                        <a href="" class="btn btn-success btn-sm" title="ลบ" data-toggle="modal"
                                            data-target="#modal-create"><i class="fas fa-plus mr-1"></i> เพิ่มข้อมูล</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="assign_tb" class="table">
                                    <thead class="thead-color">
                                        <tr class="text-center table-tr">
                                            <th>ชื่อ-สกุล ผู้รับผิดชอบ</th>
                                            <th>ตำแหน่ง</th>
                                            <th>สำนัก/กอง ที่รับผิดชอบ</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody class="table-bordered">
                                        <?php
                                        if (isset($get_admin)) {
                                            foreach ($get_admin as $row) {
                                                ?>
                                        <tr>
                                            <td>
                                                <?= $row['m_title'] . ' ' . $row['m_fname'] . ' ' . $row['m_lname']; ?>
                                            </td>
                                            <td>
                                                <?php if ($row['m_position'] != null) {
                                                            echo $row['m_position'] . ' (' . $row['cat_name'] . ')';
                                                        } else {
                                                            echo '-';
                                                        } ?>
                                            </td>
                                            <td>
                                                <?php if ($row['as_sub_id'] == '999999') {
                                                            echo '- ผู้บริหาร';
                                                        } else if ($row['as_sub_id'] == '888888') {
                                                            echo '- ราชการบำนาญ';
                                                        } else {
                                                            echo $row['g_name'];
                                                        }
                                                        ?>
                                            </td>
                                            <td class="text-center font-weight-bold">
                                                <div class="btn-group">
                                                    <a href="" class="btn btn-danger btn-sm" title="ลบ"
                                                        data-toggle="modal" data-target="#del-list"
                                                        data-url="<?= base_url('admin/assign/del/') . $row['as_id']; ?>"><i
                                                            class="fas fa-trash mr-1"></i> ลบ</a>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php }
                                        } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- Modal -->
                <div class=" modal fade" id="my-modal" tabindex="-1" role="dialog" aria-labelledby="my-modal"
                    aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <form action="<?= base_url('admin/users/del'); ?>" method="get">
                                <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <p style="padding-top: 40px;">
                                        <b>คุณต้องการลบข้อมูลรายการนี้ใช่หรือไม่?</b>
                                    </p>
                                    <input hidden type="text" id="id" name="u_id">
                                </div>
                                <div class="modal-footer" style="border-top: 0px;padding: 0px 10px 10px 0px;">
                                    <button type="submit" class="btn btn-primary btn-sm">ยืนยัน</button>
                                    <button type="button" class="btn btn-danger btn-sm"
                                        data-dismiss="modal">ยกเลิก</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>