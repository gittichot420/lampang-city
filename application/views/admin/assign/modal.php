<!-- Modal Add-->
<div class="modal fade" id="modal-create" data-backdrop="static" data-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body p-4">
                <form action="<?= base_url('admin/assign/add'); ?>" method="post">
                    <div class="form-outline float-right mt-n4">
                        <button type="button" class="close mt-n2" data-dismiss="modal" aria-label="Close">
                            <span class="txt-mark" aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="form-outline mt-3">
                        <label class="label-group" for="form2Example17">ชื่อ ผู้รับผิดชอบ</label>
                        <!-- ชื่อที่มีอยู่ต้องไม่แสดง -->
                        <select id="mem_id" name="mem_id" class=" form-control selectpicker multiple-task"
                            data-live-search="true" title="-โปรดเลือก-" style="border: solid 1px lightgrey;" required>
                            <?php foreach ($get_member as $value) { ?>
                            <option value="<?= $value['m_id'] ?>">
                                <?= $value['m_title'] . ' ' . $value['m_fname'] . ' ' . $value['m_lname']; ?>
                            </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-outline mt-3">
                        <label class="label-group" for="form2Example17">สำนัก/กอง ที่รับผิดชอบ</label>
                        <select name="group_list[]" class="form-control selectpicker border" multiple
                            data-live-search="true" data-actions-box="true" title="--โปรดเลือก--" required>
                            <?php foreach ($get_group as $value) { ?>
                            <option value="<?= $value['g_id'] ?>">
                                <?= $value['g_name']; ?>
                            </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="float-right mt-3">
                        <button type="submit" class="btn btn-success">บันทึก</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Del -->
<div class="modal fade" id="del-list" data-backdrop="static" data-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form id="del_url" action="" method="get">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <label style="padding-top: 40px;"><b>คุณต้องการลบข้อมูลรายการนี้ใช่หรือไม่?</b>
                    </label>
                </div>
                <div class="modal-footer" style="border-top: 0px;padding: 0px 10px 10px 0px;">
                    <button type="submit" class="btn btn-primary btn-sm">ยืนยัน</button>
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">ยกเลิก</button>
                </div>
            </form>
        </div>
    </div>
</div>