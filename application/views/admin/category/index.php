<style>
th:nth-child(2),
td:nth-child(2) {
    display: none;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- Left col -->
                <div class="col-md-12 mt-4">
                    <!-- title -->
                    <h2 class="float-left txt-tilte-page"> <i class="fas fa-file-alt nav-icon"></i> ประเภทสัญญาจ้าง</h2>
                    <span class="text-secondary text-sm float-right"><a href="<?= base_url('admin/dashboard') ?>"
                            class="text-secondary txt-page">หน้าหลัก</a> > สำนัก/กอง</span>
                </div>
            </div>
            <!-- CONTENT -->
            <div class="card mt-4">
                <div class="card-header">
                    <div class="card-tools">
                        <ul class="nav nav-pills ml-auto">
                            <li class="nav-item">
                                <a href="" class="btn btn-success btn-sm" title="ลบ" data-id="" data-toggle="modal"
                                    data-target="#modal-create"><i class="fas fa-plus mr-1"></i> เพิ่มข้อมูล</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="cat-table" class="table">
                            <thead class="thead-color">
                                <tr class="text-center">
                                    <th width="15%" style="text-align:center !important;">ลำดับ
                                    </th>
                                    <th></th>
                                    <th>ประเภท</th>
                                    <th>ชื่อ</th>
                                    <th width="30%"></th>
                                </tr>
                            </thead>
                            <tbody class="table-bordered">
                                <?php
                                $num = 0;
                                if (isset($get_category)) {
                                    foreach ($get_category as $row) {
                                        $num++;
                                        ?>
                                <tr>
                                    <td class="text-center font-weight-bold">
                                        <?= $num; ?>
                                    </td>
                                    <td>
                                        <?= $row['cat_type'] ?>
                                    </td>
                                    <td>
                                        <?php if ($row['cat_type'] == 0) {
                                                    echo 'ข้าราชการ';
                                                } else if ($row['cat_type'] == 1) {
                                                    echo 'ข้าราชการบำนาญ';
                                                } else if ($row['cat_type'] == 2) {
                                                    echo 'พนักงาน';
                                                } else if ($row['cat_type'] == 3) {
                                                    echo 'ผู้บริหาร';
                                                }
                                                ?>
                                    </td>
                                    <td>
                                        <?= $row['cat_name']; ?>
                                    </td>
                                    <td class="text-center font-weight-bold">
                                        <div class="btn-group">
                                            <a href="" class="btn btn-warning btn-sm" title="แก้ไข" data-toggle="modal"
                                                data-target="#edit-cat" data-id="<?= $row['cat_id']; ?>"
                                                data-name="<?= $row['cat_name']; ?>"
                                                data-type="<?= $row['cat_type'] ?>"><i class="fas fa-edit mr-1"></i>
                                                แก้ไข</a>
                                            <a href="" class="btn btn-danger btn-sm" title="ลบ" data-toggle="modal"
                                                data-target="#modal-del" data-id="<?= $row['cat_id']; ?>"><i
                                                    class="fas fa-trash mr-1"></i> ลบ</a>
                                        </div>
                                    </td>
                                </tr>
                                <?php }
                                } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    </section>
</div>
</div>
</section>
</div>