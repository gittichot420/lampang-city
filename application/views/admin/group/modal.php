<!-- Modal Add-->
<div class="modal fade" id="modal-create" data-backdrop="static" data-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body p-4">
                <form action="<?= base_url('admin/group/add'); ?>" method="post">
                    <div class="form-outline float-right mt-n4">
                        <button type="button" class="close mt-n2" data-dismiss="modal" aria-label="Close">
                            <span class="txt-mark" aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="form-outline mt-3 mb-4">
                        <label class="label-group" for="form2Example17">ชื่อสำนัก</label>
                        <input id="groups" name="groups" type="text" class="form-control" placeholder="" required />
                    </div>
                    <div class="float-right mt-3">
                        <button type="submit" class="btn btn-success">บันทึก</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Edit-->
<div class="modal fade" id="modal-edit" data-backdrop="static" data-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body p-4">
                <form action="<?= base_url('admin/group/update'); ?>" method="get">
                    <div class="form-outline float-right mt-n4">
                        <button type="button" class="close mt-n2" data-dismiss="modal" aria-label="Close">
                            <span class="txt-mark" aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <input hidden type="text" id="edit_id" name="g_id">
                    <div class="form-outline mt-3 mb-4">
                        <label class="label-group" for="form2Example17">ชื่อสำนัก</label>
                        <input id="edit_name" name="g_name" type="text" class="form-control" placeholder="" required />
                    </div>
                    <div class="float-right mt-3">
                        <button type="submit" class="btn btn-success">บันทึก</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Edit List -->
<div class="modal fade" id="edit-list" data-backdrop="static" data-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body p-4">
                <form action="<?= base_url('admin/sub_group/update'); ?>" method="get">
                    <div class="form-outline float-right mt-n4">
                        <button type="button" class="close mt-n2" data-dismiss="modal" aria-label="Close">
                            <span class="txt-mark" aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <input hidden type="text" id="edit_id" name="g_id">
                    <div class="form-outline mt-3 mb-4">
                        <label class="label-group" for="form2Example17">ชื่อสำนัก</label>
                        <input id="edit_name" name="g_name" type="text" class="form-control" placeholder="" required />
                    </div>
                    <div class="float-right mt-3">
                        <button type="submit" class="btn btn-success">บันทึก</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Del -->
<div class="modal fade" id="modal-del" data-backdrop="static" data-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="<?= base_url('admin/group/del'); ?>" method="get">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <label style="padding-top: 40px;"><b>คุณต้องการลบข้อมูลรายการนี้ใช่หรือไม่?</b>
                    </label>
                    <input hidden type="text" id="del_id" name="g_id">
                </div>
                <div class="modal-footer" style="border-top: 0px;padding: 0px 10px 10px 0px;">
                    <button type="submit" class="btn btn-primary btn-sm">ยืนยัน</button>
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">ยกเลิก</button>
                </div>
            </form>
        </div>
    </div>
</div>