<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<?php if ($this->session->flashdata('result') == 'success') {
    echo "<script>
        Swal.fire({
            icon: 'success',
            title: 'ดำเนินการสำเร็จ',
            text: '" . $this->session->flashdata('message') . "', 
        })
    </script>";
} ?>
<?php if ($this->session->flashdata('result') == 'false') {
    echo "<script>
        Swal.fire({
            icon: 'error',
            title: 'ดำเนินการไม่สำเร็จ',
            text: '" . $this->session->flashdata('message') . "',
        })
    </script>";
} ?>
<?php if ($this->session->flashdata('result') == 'duplicate') {
    echo "<script>
        Swal.fire({
            icon: 'warning',
            title: 'ดำเนินการไม่สำเร็จ',
            text: '" . $this->session->flashdata('message') . "',
        })
    </script>";
} ?>

<script>
    var Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timerProgressBar: true,
        timer: 2000,
    });

    function archiveFunction() {
        event.preventDefault(); // prevent form submit
        var form = event.target.form; // storing the form
        swal({
            title: "Are you sure?",
            text: "But you will still be able to retrieve this file.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, archive it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                if (isConfirm) {
                    form.submit(); // submitting the form when user press yes
                } else {
                    swal("Cancelled", "Your imaginary file is safe :)", "error");
                }
            });
    }
</script>