<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>เข้าสู่ระบบ</title>
    <!-- css -->
    <link rel="stylesheet" href="<?= base_url('assets/css/app.css'); ?>">
    <!-- icon menubar -->
    <!-- <link rel="icon" type="image/png" href="<?= base_url('./assets/img/logo.gif') ?>" /> -->
    <link rel="stylesheet" href="<?= base_url('assets/plugins/fontawesome-free/css/all.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/adminlte.min.css'); ?>">
</head>

<body class="bg-form">
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-12 col-lg-12">
                <div class="card bg-card mt-3 mb-3" style="border-radius: 1rem;height: 120%;">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <img src="<?= base_url('./assets/img/cover.png'); ?>" width="100%" height="131%"
                                style="border-radius: 1rem 0px 0px 1rem;" />
                        </div>
                        <div class="col-md-8">
                            <div class="card-body " style="position: relative;">
                                <div class="row mt-4">
                                    <div class="col-md-5 col-lg-5"">
                                        <form action="" method=" get">
                                        <div class="input-group">
                                            <input class="form-control mr-2" style="border-radius: 5px " type="month"
                                                name="month" value="<?= $month; ?>">
                                            <button type="submit" class="btn btn-primary">ค้นหา</button>
                                        </div>
                                        </form>
                                    </div>
                                    <div class="col-md-3 col-lg-3"></div>
                                    <div class="col-md-4 col-lg-4">
                                        <div class="float-right">
                                            <a href="<?= base_url('logout-user'); ?>"
                                                class="btn btn-danger btn-sm">ออกจากระบบ</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                                <div class="row mt-5">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <!-- group_assign -->
                                        <div class="table-responsive">
                                            <table id="table-slip" class="table" style="font-size: 13px !important;">
                                                <thead class="thead-color">
                                                    <tr class="text-center table-tr">
                                                        <th>ชื่อ-สกุล</th>
                                                        <th>สำนัก/กอง</th>
                                                        <th>สังกัด</th>
                                                        <th>ตำแหน่ง</th>
                                                        <th>รวมรับ</th>
                                                        <th>รวมจ่าย</th>
                                                        <th>คงเหลือ</th>
                                                        <th>สถานะ</th>
                                                        <?php if ($user_type['grand_total'] != null) { ?>
                                                            <th></th>
                                                        <?php } ?>
                                                    </tr>
                                                </thead>
                                                <tbody class="table-bordered">
                                                    <tr>
                                                        <td>
                                                            <?= $user_type['m_title'] . ' ' . $user_type['m_fname'] . ' ' . $user_type['m_lname']; ?>
                                                        </td>
                                                        <td>
                                                            <?php if ($user_type['sub_id'] != null || $user_type['sub_id'] != 0) {
                                                                echo $user_type['g_name'];
                                                            } else {
                                                                echo '-';
                                                            } ?>
                                                        </td>
                                                        <td>
                                                            <?php if ($user_type['sub_id'] != null || $user_type['sub_id'] != 0) {
                                                                echo $user_type['sub_name'];
                                                            } else {
                                                                echo '-';
                                                            } ?>
                                                        </td>
                                                        <td>
                                                            <?php if ($user_type['m_position'] != null || $user_type['m_position'] != 0) {
                                                                echo $user_type['m_position'];
                                                            } else {
                                                                echo '-';
                                                            } ?>
                                                        </td>
                                                        <td class="text-right">
                                                            <?= number_format($user_type['total'], 2) . ' ฿'; ?>
                                                        </td>
                                                        <td class="text-right">
                                                            <?= number_format($user_type['grand_expense'], 2) . ' ฿'; ?>
                                                        </td>
                                                        <td class="text-right">
                                                            <?= number_format($user_type['grand_total'], 2) . ' ฿'; ?>
                                                        </td>
                                                        <td class="text-center">
                                                            <?php if ($user_type['grand_total'] != null) {
                                                                echo '<span class="badge badge-success badge-status">ดำเนินการสำเร็จ</span>';
                                                            } else {
                                                                echo ' <span class="badge badge-warning badge-status">รอดำเนินการ</span>';
                                                            } ?>
                                                        </td>
                                                        <?php if ($user_type['grand_total'] != null) { ?>
                                                            <td class="text-center">
                                                                <a href=" 
                                                        <?= base_url('slip-employee/') . $month . '/' . $user_type['m_id']; ?>
                                                   " class="btn btn-primary btn-sm" target="_blank">
                                                                    <i class="fas fa-print"></i>
                                                                </a>
                                                            </td>
                                                        <?php } ?>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous">
    </script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>


<?php if ($this->session->flashdata('result') == 'success') {
    echo "<script>
        Swal.fire({
            icon: 'success',
            title: 'สำเร็จ',
            text: '" . $this->session->flashdata('message') . "', 
        })
    </script>";
} ?>
<?php if ($this->session->flashdata('result') == 'false') {
    echo "<script>
        Swal.fire({
            icon: 'error',
            title: 'ผิดพลาด',
            text: '" . $this->session->flashdata('message') . "',
        })
    </script>";
} ?>

</html>