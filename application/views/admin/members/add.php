<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- Left col -->
                <div class="col-md-12 mt-4">
                    <!-- title -->
                    <h2 class="float-left txt-tilte-page"><i class="fa fa-users nav-icon"></i> เพิ่มผู้ใช้</h2>
                    <span class="text-secondary text-sm float-right"><a href="<?= base_url('admin/dashboard') ?>"
                            class="text-secondary txt-page">หน้าหลัก</a> > <a href="<?= base_url('admin/members') ?>"
                            class="text-secondary txt-page"> ผู้ใช้ </a> > เพิ่มผู้ใช้</span>
                </div>
            </div>
            <!-- CONTENT -->
            <div class="card mt-4">
                <div class="card-body">
                    <form action="<?= base_url('admin/members/add'); ?>" method="post">
                        <!-- row -->
                        <div class="row mt-4 px-3">
                            <div class="col-sm-12">
                                <div class="form-outline mb-3">
                                    <label class="form-label form-regis" for="form2Example17">เลขบัตรประจำตัวประชาชน
                                        *</label>
                                    <div class="btn-group d-flex">
                                        <input id="card_id" name="card_id" type="number"
                                            onKeyPress="if(this.value.length==13) return false;" maxlength="13"
                                            class="form-control form-control" required style="width: 80%;" />
                                        <button type="button" class="btn btn-info btn-sm" id="btnVerify" onclick="verifyMember();$(this).html('processing...<i class=\'fa fa-cog faa-spin fa-spin fa-spinner\'></i>');">ยืนยัน</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-outline mb-3">
                                    <label class="form-label form-regis" for="form2Example17">คำนำหน้า*</label>
                                    <select class="form-control" id="title" name="title"
                                        required>
                                        <option value="" disabled selected>-เลือก-</option>
                                        <option value="นาย">นาย</option>
                                        <option value="นาง">นาง</option>
                                        <option value="นางสาว">นางสาว</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-outline mb-3">
                                    <label class="form-label form-regis" for="form2Example17">ชื่อ*</label>
                                    <input id="fname" name="fname" type="text"
                                        class="form-control form-control" required />
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="form-outline mb-3">
                                    <label class="form-label form-regis" for="form2Example17">นามสกุล*</label>
                                    <input id="lname" name="lname" type="text"
                                        class="form-control form-control" required />
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-outline mb-3">
                                    <label class="form-label form-regis" for="form2Example17">ตำแหน่ง*</label>
                                    <input id="position" name="position" type="text"
                                        onKeyPress="if(this.value.length==13) return false;"
                                        class="form-control form-control" required />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label class="form-label form-regis" for="form2Example17">อีเมล* </label>
                                    <input id="email" name="email" type="email"
                                        class="form-control form-control" onkeyup='checkEmail();' required />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label class="form-label form-regis" for="form2Example17">ยืนยันอีเมล* </label>
                                    <input id="confirm_email" name="confirm_email" type="email"
                                        class="form-control form-control" onkeyup='checkEmail();' required />
                                    <small id='messageEmail' class="text-center"></small>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label class="form-label form-regis" for="form2Example17">รหัสผ่าน
                                        *</label>
                                    <input id="password" name="password" type="password"
                                        class="form-control form-control" onkeyup='checkPass();'
                                        pattern="^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$"
                                        title="ต้องมีตัวพิมพ์ใหญ่,ตัวพิมพ์เล็ก,ตัวเลขอย่างน้อยหนึ่งตัวและไม่น้อยกว่า 8 ตัวอักษร"
                                        required />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label class="form-label form-regis" for="form2Example17">ยืนยันรหัสผ่าน
                                        *
                                    </label>
                                    <input id="confirm_password" name="confirm_password" type="password"
                                        class="form-control form-control" onkeyup='checkPass();'
                                        pattern="^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$" />
                                    <small id='message' class="text-center"></small>
                                </div>
                            </div>
                            <p><small id="emailHelp"
                                    class="form-text text-muted mb-3">*รหัสผ่านต้องเป็นภาษาอังกฤษตัวพิมพ์ใหญ่,
                                    ตัวพิมพ์เล็กและตัวเลขมากกว่า
                                    8 ตัวอักษร.</small></p>
                        </div>
                        <!-- end row -->
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-success ">บันทึก</button>
                                <button type="reset" class="btn btn-primary ">ล้างข้อมูล</button>
                            </div>
                        </div>
                        <!-- end row -->
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    function verifyMember() {
        var card_id = $("#card_id").val();
        $.ajax({
            url: '/auth/checkID',
            type: 'POST',
            data: {
                card_id: card_id
            },
            dateType: 'json',
            success: function(respond, textStatus, jqXHR) {
                console.log(respond);
                if (respond.success == 0) {
                    // เจอ
                    // Swal.fire({
                    //     icon: 'success',
                    //     title: respond.msg,
                    //     showConfirmButton: true,
                    //     allowOutsideClick: false
                    // });
                    $("#card_id").val(respond.i);
                    $("#title").val(respond.prefix);
                    $("#fname").val(respond.fname);
                    $("#lname").val(respond.lname);
                    $("#position").val(respond.position);
                } else if (respond.success == 1) {
                    // ไม่เจอ
                    Swal.fire({
                        icon: 'error',
                        title: respond.msg,
                        showConfirmButton: true,
                        allowOutsideClick: false
                    });
                    $("#card_id").val('');
                    $("#title").val('');
                    $("#fname").val('');
                    $("#lname").val('');
                    $("#position").val('');
                } else if (respond.success == 2) {
                    // ไม่เจอ
                    Swal.fire({
                        icon: 'error',
                        title: respond.msg,
                        showConfirmButton: true,
                        allowOutsideClick: false
                    });
                    $("#card_id").val('');
                    $("#title").val('');
                    $("#fname").val('');
                    $("#lname").val('');
                    $("#position").val('');
                }
                // $("#responseMember").html(respond.msg);

                $("#btnVerify").html('ยืนยัน');
            }
        });
    }

    var checkPass = function() {
        var pass = $('#password').val();
        var confirm_password = $('#confirm_password').val();

        if (pass && confirm_password != null) {
            if (pass == confirm_password) {
                $('#message').css('color', 'green');
                $('#message').html(' รหัสผ่านตรง');
            } else {
                $('#message').css('color', 'red');
                $('#message').html(' รหัสผ่านไม่ตรงกัน');
            }
        } else {
            $('#message').html('');
        }
    }

    var checkEmail = function() {
        var email = $('#email').val();
        var confirm_email = $('#confirm_email').val();

        if (email && confirm_email != null) {
            if (email == confirm_email) {
                $('#messageEmail').css('color', 'green');
                $('#messageEmail').html(' อีเมลตรงกัน');
            } else {
                $('#messageEmail').css('color', 'red');
                $('#messageEmail').html(' อีเมลตรงกันไม่ตรงกัน');
            }
        } else {
            $('#messageEmail').html('');
        }
    }
</script>