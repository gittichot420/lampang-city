<!-- Content Wrapper. Contains page content -->
<style>
    /* #get_users td:nth-child(7),
th:nth-child(7) {
    display: none;
} */
</style>

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">

            </div>
            <div class="row">
                <!-- Left col -->
                <div class="col-md-12 mt-4">
                    <!-- title -->
                    <h2 class="float-left txt-tilte-page"><i class="fa fa-users nav-icon"></i> ผู้ใช้
                    </h2>
                    <span class="text-secondary text-sm float-right"><a href="<?= base_url('admin/dashboard') ?>"
                            class="text-secondary txt-page">หน้าหลัก</a> > ผู้ใช้</span>
                </div>
            </div>
            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <section class="col-md-12">
                    <!-- DIRECT CHAT -->
                    <div class="card mt-4">
                        <div class="card-header">
                            <div class="card-tools">
                                <ul class="nav nav-pills ml-auto">
                                    <li class="nav-item">
                                        <a class="btn btn-success btn-sm" href="<?= base_url('admin/members/add'); ?>"><i
                                                class="fas fa-plus mr-1"></i> เพิ่มข้อมูล</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="get_members" class="table" style="font-size: 13px !important;">
                                    <thead class="thead-color">
                                        <tr class="text-center table-tr">
                                            <th width="7%">
                                                ลำดับ
                                            </th>
                                            <th>ชื่อ-สกุล</th>
                                            <th>ตำแหน่ง</th>
                                            <th>เลขบัตรประจำตัวประชาชน</th>
                                            <th>อีเมล</th>
                                            <th>สถานะ</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody class="table-bordered">
                                        <?php
                                        $num = 0;
                                        if (isset($get_users)) {
                                            foreach ($get_users as $row) {
                                                $num++;
                                        ?>
                                                <tr>
                                                    <td class="text-center font-weight-bold">
                                                        <?= $num; ?>
                                                    </td>
                                                    <td>
                                                        <?= $row['u_title'] . ' ' . $row['u_fname'] . ' ' . $row['u_lname']; ?>
                                                    </td>
                                                    <td>
                                                        <?= $row['u_position'] ?>
                                                    </td>
                                                    <td class="text-center">
                                                        <?= $row['u_user'] ?>
                                                    </td>
                                                    <td>
                                                        <?= $row['u_email'] ?>
                                                    </td>
                                                    <td class="text-center">
                                                        <?php if ($row['u_status'] == 0) {
                                                            echo 'ปิดใช้งาน';
                                                        } else if ($row['u_status'] == 1) {
                                                            echo 'เปิดใช้งาน';
                                                        } ?>
                                                    </td>
                                                    <td class="text-center font-weight-bold">
                                                        <div class="btn-group">
                                                            <a href="<?= base_url('admin/members/edit/') . $row['u_id']; ?>"
                                                                class="btn btn-warning btn-sm" title="แก้ไข">
                                                                <i class=" fas fa-edit mr-1"></i> แก้ไข</a>
                                                            <a href="" class="btn btn-danger btn-sm" title="ลบ"
                                                                data-toggle="modal" data-target="#del-list"
                                                                data-id="<?= $row['u_id']; ?>"
                                                                data-url="<?= base_url('admin/members/del/') . $row['u_id']; ?>"><i
                                                                    class="fas fa-trash mr-1"></i> ลบ</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                        <?php }
                                        } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- Modal Del -->
                <div class="modal fade" id="del-list" data-backdrop="static" data-keyboard="false" tabindex="-1"
                    aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <form id="del_url" action="" method="post">
                                <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <label style="padding-top: 40px;"><b>คุณต้องการลบข้อมูลรายการนี้ใช่หรือไม่?</b>
                                    </label>
                                </div>
                                <div class="modal-footer" style="border-top: 0px;padding: 0px 10px 10px 0px;">
                                    <button type="submit" class="btn btn-primary btn-sm">ยืนยัน</button>
                                    <button type="button" class="btn btn-danger btn-sm"
                                        data-dismiss="modal">ยกเลิก</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>