<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- Left col -->
                <div class="col-md-12 mt-4">
                    <!-- title -->
                    <h2 class="float-left txt-tilte-page"><i class="fa fa-users nav-icon"></i> แก้ไขสมาชิก</h2>
                    <span class="text-secondary text-sm float-right"><a href="<?= base_url('admin/dashboard') ?>"
                            class="text-secondary txt-page">หน้าหลัก</a> > <a href="<?= base_url('admin/members') ?>"
                            class="text-secondary txt-page"> ผู้ใช้ </a> > แก้ไขผู้ใช้</span>
                </div>
            </div>
            <!-- CONTENT -->
            <div class="card mt-4">
                <div class="card-body">
                    <form action="<?= base_url('admin/members/edit/') . $users_id['u_id']; ?>" method="get">
                        <!-- row -->
                        <div class="row mt-4 px-3">
                            <div class="col-sm-6">
                                <div class="form-outline mb-3">
                                    <label class="form-label form-regis" for="form2Example17">เลขบัตรประจำตัวประชาชน
                                        *</label>
                                    <div class="btn-group d-flex">
                                        <input id="card_id" name="card_id" type="number"
                                            onKeyPress="if(this.value.length==13) return false;" maxlength="13"
                                            class="form-control form-control" value="<?= $users_id['u_user'] ?>" readonly required />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label class="form-label form-regis" for="form2Example17">อีเมล* </label>
                                    <input id="email" name="email" type="email"
                                        class="form-control form-control" onkeyup='checkEmail();' value="<?= $users_id['u_email'] ?>" readonly required />
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-outline mb-3">
                                    <label class="form-label form-regis" for="form2Example17">คำนำหน้า*</label>
                                    <select class="form-control" id="title" name="title"
                                        required>

                                        <option value="" disabled>-เลือก-</option>
                                        <option value="นาย" <?php
                                                            if ($users_id['u_title'] == 'นาย') {
                                                                echo 'selected';
                                                            } ?>>นาย</option>
                                        <option value="นาง" <?php
                                                            if ($users_id['u_title'] == 'นาง') {
                                                                echo 'selected';
                                                            } ?>>นาง</option>
                                        <option value="นางสาว" <?php
                                                                if ($users_id['u_title'] == 'นางสาว') {
                                                                    echo 'selected';
                                                                } ?>>นางสาว</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-outline mb-3">
                                    <label class="form-label form-regis" for="form2Example17">ชื่อ*</label>
                                    <input id="fname" name="fname" type="text"
                                        class="form-control form-control" value="<?= $users_id['u_fname'] ?>" required />
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="form-outline mb-3">
                                    <label class="form-label form-regis" for="form2Example17">นามสกุล*</label>
                                    <input id="lname" name="lname" type="text"
                                        class="form-control form-control" value="<?= $users_id['u_lname'] ?>" required />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline mb-3">
                                    <label class="form-label form-regis" for="form2Example17">ตำแหน่ง*</label>
                                    <input id="position" name="position" type="text"
                                        onKeyPress="if(this.value.length==13) return false;"
                                        class="form-control form-control" value="<?= $users_id['u_position'] ?>" required />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline mb-3">
                                    <label class="form-label form-regis" for="form2Example17">สถานะ</label>
                                    <input id="status" name="status" type="checkbox"
                                        class="form-control form-control mt-2" value="1" <?php if ($users_id['u_status'] == 1) {
                                                                                                echo 'checked';
                                                                                            } ?> style=" width: 10%; height: calc(1.25rem + 2px);;" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label class="form-label form-regis" for="form2Example17">รหัสผ่าน
                                        *</label>
                                    <input id="password" name="password" type="password"
                                        class="form-control form-control" onkeyup='checkPass();'
                                        pattern="^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$"
                                        title="ต้องมีตัวพิมพ์ใหญ่,ตัวพิมพ์เล็ก,ตัวเลขอย่างน้อยหนึ่งตัวและไม่น้อยกว่า 8 ตัวอักษร" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label class="form-label form-regis" for="form2Example17">ยืนยันรหัสผ่าน
                                        *
                                    </label>
                                    <input id="confirm_password" name="confirm_password" type="password"
                                        class="form-control form-control" onkeyup='checkPass();'
                                        pattern="^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$" />
                                    <small id='message' class="text-center"></small>
                                </div>
                            </div>
                            <p><small id="emailHelp"
                                    class="form-text text-muted mb-3">*รหัสผ่านต้องเป็นภาษาอังกฤษตัวพิมพ์ใหญ่,
                                    ตัวพิมพ์เล็กและตัวเลขมากกว่า
                                    8 ตัวอักษร.</small></p>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-success ">บันทึก</button>
                                <button type="reset" class="btn btn-primary ">ล้างข้อมูล</button>
                            </div>
                        </div>
                        <!-- end row -->
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    var checkPass = function() {
        var pass = $('#password').val();
        var confirm_password = $('#confirm_password').val();

        if (pass && confirm_password != null) {
            if (pass == confirm_password) {
                $('#message').css('color', 'green');
                $('#message').html(' รหัสผ่านตรง');
            } else {
                $('#message').css('color', 'red');
                $('#message').html(' รหัสผ่านไม่ตรงกัน');
            }
        } else {
            $('#message').html('');
        }
    }
</script>