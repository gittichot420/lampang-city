<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- Left col -->
                <div class="col-md-12 mt-4">
                    <!-- title -->
                    <h2 class="float-left txt-tilte-page"><i class="fa fa-users nav-icon"></i> เพิ่มสมาชิก</h2>
                    <span class="text-secondary text-sm float-right"><a href="<?= base_url('admin/dashboard') ?>"
                            class="text-secondary txt-page">หน้าหลัก</a> ><a href="<?= base_url('admin/users') ?>"
                            class="text-secondary txt-page">สมาชิก</a> > เพิ่มสมาชิก</span>
                </div>
            </div>
            <!-- CONTENT -->
            <div class="card mt-4">
                <div class="card-body">
                    <form action="<?= base_url('admin/users/add'); ?>" method="post">
                        <!-- row -->
                        <div class="row">
                            <div class="col-md-12">
                                <!-- row1 -->
                                <div class="row">
                                    <div class="col-md-2">
                                        <!-- คำนำหน้า -->
                                        <div class="form-outline mb-3">
                                            <label class="form-label form-regis" for="form2Example17">คำนำหน้า
                                                *</label>
                                            <select class="form-control form-control-sm" id="title" name="title"
                                                required>
                                                <option value="" disabled selected>-เลือก-</option>
                                                <option value="นาย">นาย</option>
                                                <option value="นาง">นาง</option>
                                                <option value="นางสาว">นางสาว</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <!-- ชื่อ -->
                                        <div class="form-outline mb-3">
                                            <label class="form-label form-regis" for="form2Example17">ชื่อ
                                                *</label>
                                            <input id="first_name" name="first_name" type="text"
                                                class="form-control form-control-sm" required />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <!-- นามสกุล -->
                                        <div class="form-outline mb-3">
                                            <label class="form-label form-regis" for="form2Example17">นามสกุล
                                                *</label>
                                            <input id="last_name" name="last_name" type="text"
                                                class="form-control form-control-sm" required />
                                        </div>
                                    </div>
                                    <div class="col-md-1"></div>
                                    <div class="col-md-5">
                                        <!-- สิทธิ์ -->
                                        <div class="form-outline mb-3">
                                            <label class="form-label form-regis" for="form2Example17">สิทธิ์ *</label>
                                            <select class="form-control form-control-sm" id="status" name="status"
                                                required>
                                                <option value="" disabled selected>-เลือก-</option>
                                                <?php if ($this->session->userdata('users')['status']== 0) { ?>
                                                <option value="0">ผู้ดูแลระบบสูงสุด</option>
                                                <?php } ?>
                                                <option value="1">ผู้ดูแลระบบ</option>
                                                <option value="2" selected>สมาชิก</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!-- row2 -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <!-- เลขบัตรประจำตัวประชาชน -->
                                        <div class="form-outline mb-3">
                                            <label class="form-label form-regis"
                                                for="form2Example17">เลขบัตรประจำตัวประชาชน
                                                *</label>
                                            <input id="card_id" name="card_id" type="number"
                                                onKeyPress="if(this.value.length==13) return false;" maxlength="13"
                                                class="form-control form-control-sm" required />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <!-- เบอร์โทร -->
                                        <div class="form-outline mb-3">
                                            <label class="form-label form-regis" for="form2Example17">เบอร์โทร</label>
                                            <input id="phone" name="phone" type="number"
                                                onKeyPress="if(this.value.length==13) return false;"
                                                class="form-control form-control-sm" />
                                        </div>
                                    </div>
                                </div>
                                <!-- row3 -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-outline mb-3">
                                            <label class="form-label form-regis" for="form2Example17">สำนัก/กอง
                                            </label>
                                            <select id="sub_group" name="sub_group"
                                                class="form-control form-control-sm selectpicker multiple-task"
                                                data-live-search="true" title="-โปรดเลือก-"
                                                style="border: solid 1px lightgrey;">
                                                <option value="">ไม่ระบุ</option>
                                                <?php foreach ($get_subGroup as $row) { ?>
                                                <option id="<?= $row['sub_id'] ?>" value="<?= $row['sub_id'] ?>">
                                                    <?= $row['sub_name'] . ' (' . $row['g_name'] . ')' ?>
                                                    <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!-- row4 -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-outline mb-3">
                                            <label class="form-label form-regis" for="form2Example17">ประเภทสัญญาจ้าง *
                                            </label>
                                            <select id="category" name="category"
                                                class=" form-control form-control-sm selectpicker multiple-task"
                                                data-live-search="true" title="-โปรดเลือก-"
                                                style="border: solid 1px lightgrey;" required>
                                                <?php foreach ($get_category as $row) { ?>
                                                <option id="<?= $row['cat_id'] ?>" value="<?= $row['cat_id'] ?>">
                                                    <?= $row['cat_name']; ?>
                                                    <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!-- row5 -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-outline mb-3">
                                            <label class="form-label form-regis" for="form2Example17">ตำแหน่ง </label>
                                            <input id="position" name="position" type="text"
                                                class="form-control form-control-sm" />
                                        </div>
                                    </div>
                                </div>
                                <!-- row6 -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-outline mb-3">
                                            <label class="form-label form-regis" for="form2Example17">เงินเดือน </label>
                                            <input id="salary" name="salary" type="text"
                                                class="form-control form-control-sm" />
                                        </div>
                                    </div>
                                </div>
                                <!-- row7 -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-outline mb-3">
                                            <label class="form-label form-regis" for="form2Example17">รหัสผ่าน
                                            </label>
                                            <input id="password" name="password" type="password"
                                                class="form-control form-control-sm" onkeyup='check();'
                                                pattern="^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$"
                                                title="ต้องมีตัวพิมพ์ใหญ่,ตัวพิมพ์เล็ก,ตัวเลขอย่างน้อยหนึ่งตัวและไม่น้อยกว่า 8 ตัวอักษร" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-outline mb-3">
                                            <label class="form-label form-regis" for="form2Example17">ยืนยันรหัสผ่าน
                                            </label>
                                            <input id="confirm_password" name="confirm_password" type="password"
                                                class="form-control form-control-sm" onkeyup='check();'
                                                pattern="^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$" />
                                        </div>
                                    </div>
                                </div>
                                <label id='message' class="text-center"></label>
                                <p><small id="emailHelp"
                                        class="form-text text-muted mb-3">*รหัสผ่านต้องเป็นภาษาอังกฤษตัวพิมพ์ใหญ่,
                                        ตัวพิมพ์เล็กและตัวเลขมากกว่า
                                        8 ตัวอักษร.</small></p>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-success ">บันทึก</button>
                                        <button type="reset" class="btn btn-primary ">ล้างข้อมูล</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>