<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/userguide3/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'auth/login_users';
$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;


$route['auth/login'] = 'auth/login_users';
$route['register'] = 'auth/register_users';
$route['forgot-pass'] = 'auth/forgot_pass';
$route['reset-pass'] = 'auth/reset_pass';

$route['auth/checkID'] = 'auth/checkID';


$route['slips-employee'] = 'admin/employee/index';
$route['slip-employee/(:any)/(:any)'] = 'admin/employee/slip_employee/$1/$2';


$route['admin'] = 'auth/login_admin';
$route['admin/login'] = 'auth/login_admin';
$route['admin/register'] = 'auth/register_admin';
$route['logout'] = 'auth/logout';
$route['logout-user'] = 'auth/logout_user';

// dashboard
$route['admin/dashboard'] = 'admin/dashboard/index';
$route['admin/dashboard/(:num)'] = 'admin/slip/index/$1';
$route['admin/slip-admin/(:any)/(:any)'] = 'admin/slip/slip_admin/$1/$2';
$route['admin/slip-all'] = 'admin/slip/slip_all';
$route['admin/print-slip/(:any)/(:any)'] = 'admin/slip/print_slip/$1/$2';

// 999999 || 888888
$route['admin/slip-add/(:any)/(:any)'] = 'admin/slip/slip_add/$1/$2';
$route['admin/slip/add/(:any)'] = 'admin/slip/add/$1';

$route['admin/slip-update/(:any)/(:any)'] = 'admin/slip/slip_update/$1/$2';
$route['admin/slip/update/(:any)'] = 'admin/slip/update/$1';

// group
$route['admin/group'] = 'admin/group/index';
$route['admin/group/add'] = 'admin/group/create';
$route['admin/group/update'] = 'admin/group/update';
$route['admin/group/del'] = 'admin/group/delete';

// list
$route['admin/group/list/(:any)'] = 'admin/subGroup/index/$1';
$route['admin/sub-group/add'] = 'admin/subGroup/create';
$route['admin/sub-group/update/(:any)'] = 'admin/subGroup/update/$1';
$route['admin/sub-group/del/(:any)'] = 'admin/subGroup/delete/$1';

// users
$route['admin/users'] = 'admin/users/index';
$route['admin/users/add'] = 'admin/users/add';
$route['admin/users/edit/(:num)'] = 'admin/users/edit/$1';
$route['admin/users/del/(:num)'] = 'admin/users/del/$1';

// members
$route['admin/members'] = 'admin/members/index';
$route['admin/members/add'] = 'admin/members/add';
$route['admin/members/edit/(:num)'] = 'admin/members/edit/$1';
$route['admin/members/del/(:num)'] = 'admin/members/del/$1';

// category
$route['admin/category'] = 'admin/category/index';
$route['admin/category/add'] = 'admin/category/create';
$route['admin/category/edit'] = 'admin/category/update';
$route['admin/category/del'] = 'admin/category/delete';

// assign
$route['admin/assign'] = 'admin/assign/index';
$route['admin/assign/add'] = 'admin/assign/create';
$route['admin/assign/edit'] = 'admin/assign/update';
$route['admin/assign/del/(:num)'] = 'admin/assign/delete/$1';

$route['admin/resetData'] = 'admin/users/reset_data';
